package se.ikea.addonapps.vpcservice.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import msc.selling.common.shoppingbag._2.AttachmentRefTypeEnumSimpleType;
import msc.selling.common.shoppingbag._2.AttachmentTypeEnumSimpleType;
import msc.selling.common.shoppingbag._2.FindShoppingBagCriteriaComplexType;
import msc.selling.common.shoppingbag._2.GetShoppingBagCriteriaListComplexType;
import msc.selling.common.shoppingbag._2.ObjectFactory;
import msc.selling.common.shoppingbag._2.ShoppingBagAttachmentComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagAttachmentListComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagListComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagTypeEnumSimpleType;
import se.ikea.addonapps.vpcservice.model.Configuration;

public class JaxbHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(JaxbHelper.class);

	private static final String VISUAL_PRODUCT_CONFIGURATION_SERVICE_CTX_PATH = "com.ikea.cem.iows.visualproductconfigurationservice._1:msc.selling.common.shoppingbag._2";

	private static final String SHOPPINGBAG_SERVICE_CTX_PATH = "com.ikea.vpcservice.vpcshoppingbagservice._1";
	
	private JaxbHelper() {}
	
	/**
	 * @return the jaxbContext
	 * @throws JAXBException 
	 */
	public static JAXBContext getJaxbContext(String contextPath) throws JAXBException {
		JAXBContext jaxbContext = null;
		try {
			jaxbContext = JAXBContext.newInstance(contextPath);
		} catch (JAXBException e) {
			logger.error("Unable to create jaxbcontext [{}]", contextPath, e);
			throw e;
		}
		return jaxbContext;
	}
	
	public static JAXBContext getJAXBContextShoppingBagService() throws JAXBException {
		return getJaxbContext(SHOPPINGBAG_SERVICE_CTX_PATH);
	}
	
	public static JAXBContext getJAXBContextVisualProductConfigurationService() throws JAXBException {
		return getJaxbContext(VISUAL_PRODUCT_CONFIGURATION_SERVICE_CTX_PATH);
	}

	/**
	 * @param object
	 * @return
	 * @throws JAXBException 
	 */
	public static <T> String marshallShoppingBagService(T object) throws JAXBException {
		Marshaller marshaller = getJAXBContextShoppingBagService().createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(object, stringWriter);
		return stringWriter.toString();
	}
	
		
	public static <T> T  unmarshall(Class<T> someClass, String xml) throws JAXBException, IOException, ParserConfigurationException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
		Node xmlPartTree = doc.getFirstChild();
		Unmarshaller unmarshaller = JAXBContext.newInstance(someClass).createUnmarshaller();
		
		JAXBElement<T> shoppingBagCritListElement = (JAXBElement<T>) unmarshaller
				.unmarshal(xmlPartTree,someClass);
		return (T) shoppingBagCritListElement.getValue();
	}

	public static ShoppingBagComplexType generateShoppingBag(Configuration config, String bagUrl, String configDeepLinkUrl, String shortUrl) throws JAXBException, IOException, ParserConfigurationException, SAXException {
		ObjectFactory objectFactory = new ObjectFactory();
		ShoppingBagComplexType shoppingBagComplexType = objectFactory.createShoppingBagComplexType();
		shoppingBagComplexType.setBagId(config.getFriendlyId());
		shoppingBagComplexType.setBagSource(config.getSource());
		shoppingBagComplexType.setBagType(ShoppingBagTypeEnumSimpleType.PRODUCTCONFIG);
		shoppingBagComplexType.setBagName(config.getSource()+" ShoppingBag");
		shoppingBagComplexType.setBagUrl(bagUrl);

		// attachment representing a deep-link to open the configuration
		// inside the source configurator
		ShoppingBagAttachmentListComplexType attachmentList = new ShoppingBagAttachmentListComplexType();
		shoppingBagComplexType.setShoppingBagAttachmentList(attachmentList);
		
		ShoppingBagAttachmentComplexType attachmentDL = new ShoppingBagAttachmentComplexType();
		attachmentDL.setAttachmentRef(configDeepLinkUrl);
		attachmentDL.setAttachmentRefType(AttachmentRefTypeEnumSimpleType.URL);
		attachmentDL.setAttachmentName("APPLICATION_DEEPLINK");
		attachmentDL.setAttachmentType(AttachmentTypeEnumSimpleType.DOCUMENT);
		attachmentList.getShoppingBagAttachment().add(attachmentDL);
		
		ShoppingBagAttachmentComplexType attachmentSL= new ShoppingBagAttachmentComplexType();
		attachmentSL.setAttachmentRef(shortUrl);
		attachmentSL.setAttachmentRefType(AttachmentRefTypeEnumSimpleType.URL);
		attachmentSL.setAttachmentName("SHORT_URL");
		attachmentSL.setAttachmentType(AttachmentTypeEnumSimpleType.DOCUMENT);
		attachmentList.getShoppingBagAttachment().add(attachmentSL);

		shoppingBagComplexType.setShoppingBagSectionList(config.getShoppingBagSectionListXml());
		return shoppingBagComplexType;
	}
	
	public static ShoppingBagListComplexType generateShoppingBagList(List<ShoppingBagComplexType> bags) {
		ObjectFactory objectFactory = new ObjectFactory();
		ShoppingBagListComplexType shoppingBagListComplexType = objectFactory.createShoppingBagListComplexType();
		for (ShoppingBagComplexType bag : bags) {
			shoppingBagListComplexType.getShoppingBag().add(bag);
		}
		return shoppingBagListComplexType;
	}

	public static GetShoppingBagCriteriaListComplexType getGetShoppingBagCriteriaList(String str) throws JAXBException {
		StringReader reader = new StringReader(str);
		JAXBContext jaxbContext = JaxbHelper.getJAXBContextShoppingBagService();
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		JAXBElement<GetShoppingBagCriteriaListComplexType> jaxbElement = (JAXBElement<GetShoppingBagCriteriaListComplexType>) unmarshaller
				.unmarshal(reader);
		return jaxbElement.getValue();
	}

	public static FindShoppingBagCriteriaComplexType getFindShoppingBagCriteria(String payload) throws JAXBException {
		StringReader reader = new StringReader(payload);
		JAXBContext jaxbContext = JaxbHelper.getJAXBContextShoppingBagService();
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		JAXBElement<FindShoppingBagCriteriaComplexType> jaxbElement = (JAXBElement<FindShoppingBagCriteriaComplexType>) unmarshaller
				.unmarshal(reader);
		return jaxbElement.getValue();
	}

	public static ShoppingBagListComplexType generateShoppingBagHeadInList(Configuration config, String createShoppingBagUrl) {
		ObjectFactory objectFactory = new ObjectFactory();
		ShoppingBagComplexType shoppingBagComplexType = objectFactory.createShoppingBagComplexType();
		shoppingBagComplexType.setBagId(config.getFriendlyId());
		shoppingBagComplexType.setBagSource(config.getSource());
		shoppingBagComplexType.setBagType(ShoppingBagTypeEnumSimpleType.PRODUCTCONFIG);
		shoppingBagComplexType.setBagName(config.getSource()+" ShoppingBag");
		shoppingBagComplexType.setBagUrl(createShoppingBagUrl);
		shoppingBagComplexType.setShoppingBagSectionList(objectFactory.createShoppingBagSectionListComplexType());
		shoppingBagComplexType.setShoppingBagAttachmentList(objectFactory.createShoppingBagAttachmentListComplexType());

		ShoppingBagListComplexType shoppingBagListComplexType = objectFactory.createShoppingBagListComplexType();
		shoppingBagListComplexType.getShoppingBag().add(shoppingBagComplexType);
		return shoppingBagListComplexType;
	}

}
