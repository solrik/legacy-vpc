package se.ikea.addonapps.vpc.common.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonRootName(value = "emailContent")
public class EmailContent {

	private String subject;
	private String body;
	private Attachment[] attachments;
	
	@JsonGetter
	public String getSubject() {
		return subject;
	}

	@JsonSetter
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@JsonGetter
	public String getBody() {
		return body;
	}

	@JsonSetter
	public void setBody(String body) {
		this.body = body;
	}

	@JsonGetter
	public Attachment[] getAttachments() {
		return attachments;
	}

	@JsonSetter
	public void setAttachments(Attachment[] attachments) {
		this.attachments = attachments;
	}
}
