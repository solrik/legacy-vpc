package se.ikea.addonapps.vpc.common.util;

public class LogHelper {

	private LogHelper() {}
	
	public static String getSnippetOfLongString(String s, int amountOfChars) {
		if (s == null)
			return null;
		if (s.length()<=amountOfChars)
			return s;
		return s.substring(0, amountOfChars/2)+"..."+s.substring(s.length()-(amountOfChars/2), s.length()-1);
	}	
}
