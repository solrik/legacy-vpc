package com.ikea.dexf.vpc;

import com.ikea.dexf.vpc.app.VpcApplication;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = VpcApplication.class)
@TestPropertySource(locations = "classpath:application.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class VpcApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(VpcApplicationTests.class);

    @LocalServerPort
    private int port;

    @BeforeClass
    public static void beforeClass() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Before
    public void beforeTest() throws Exception {
        RestAssured.port = port;
    }

    @Test
    public void ping() {
        //given().when().get("/vpc/v1/ping").then().statusCode(HttpStatus.OK.value());
    }


}
