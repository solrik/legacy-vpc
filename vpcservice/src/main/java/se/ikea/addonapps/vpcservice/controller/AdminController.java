package se.ikea.addonapps.vpcservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.vpcservice.model.VpConfiguration;
import se.ikea.addonapps.vpcservice.exception.ResourceNotFoundException;
import se.ikea.addonapps.vpcservice.model.Configuration;
import se.ikea.addonapps.vpcservice.service.ConfigurationService;
import se.ikea.addonapps.vpcservice.util.ModelMapper;

@Controller
@RequestMapping("/")
public class AdminController {

	@Autowired
	private ConfigurationService configService;
	
	@RequestMapping(value="/configuration/count", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String getConfigurationCount() {
		return configService.getConfigurationCount().toString();
	}
	
	@RequestMapping(value="/configuration/latest", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<VpConfiguration> getLatestConfiguration() {
		Configuration config = configService.getLatestConfiguration();
		if(config == null) {
			throw new ResourceNotFoundException(); 
		}

		VpConfiguration vpConfig = ModelMapper.toVpConfiguration(config);
		return new ResponseEntity<>(vpConfig, HttpStatus.OK);
	}

}
