package se.ikea.addonapps.vpcservice.jpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import se.ikea.addonapps.vpcservice.model.Configuration;

/**
 * @author shashi
 *
 */
//public interface VPCConfigurationRepository extends VPCRepository<Configuration, Long> {
public interface VPCConfigurationRepository extends Repository<Configuration, Long> {
	
	Configuration save(Configuration entity);
	
	Configuration findById(long id);
	
	Configuration findByHash(String hash);

	void deleteById(long id);
	
	@Query(value="SELECT COUNT(C) FROM Configuration C")
	Long countConfigurations();

	Configuration findTop1ByOrderByIdDesc();

}
