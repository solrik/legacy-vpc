package se.ikea.addonapps.vpcservice.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

import io.swagger.vpcservice.model.VpConfiguration;
import io.swagger.vpcservice.model.VpConfigurationRef;
import msc.selling.common.shoppingbag._2.ShoppingBagSectionListComplexType;
import se.ikea.addonapps.vpcservice.config.AppPropertiesVpcService;
import se.ikea.addonapps.vpcservice.exception.InvalidConfigurationContentException;
import se.ikea.addonapps.vpcservice.exception.ResourceNotFoundException;
import se.ikea.addonapps.vpcservice.model.Configuration;
import se.ikea.addonapps.vpcservice.service.ConfigurationService;
import se.ikea.addonapps.vpcservice.service.UrlService;
import se.ikea.addonapps.vpcservice.util.JaxbHelper;
import se.ikea.addonapps.vpcservice.util.ModelMapper;


@Controller
@RequestMapping("/v1/{ru:[a-z]{2}}/{lc:[a-z]{2}}/configuration")
public class ConfigurationRestController {

	@Autowired
	private ConfigurationService configService;
	
	@Autowired
	private UrlService configuratorUrlService;
	
	@Autowired
	private AppPropertiesVpcService props;
	
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationRestController.class);

	
	@RequestMapping(value="", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<VpConfigurationRef> post(@PathVariable String ru, @PathVariable String lc, @RequestBody VpConfiguration vpConfig, HttpServletResponse httpResponse) throws InvalidConfigurationContentException {
		long startTime = System.currentTimeMillis();
		logger.debug("PostConfiguration input ru={}, lc={}", ru, lc);
		logger.trace("PostConfiguration payload {}", vpConfig);
		validateInputVpConfig(vpConfig);
		Configuration inputConfig = ModelMapper.fromVpConfiguration(vpConfig, ru, lc);//XmlToPojoConverter.toPojo(vpcConfigComplexType, ru, lc, true);
		
		Configuration config = null;
		try {	
			config = configService.store(ru,lc,inputConfig);
		} catch (InvalidConfigurationContentException e) {
			logger.warn("Invalid [PostConfiguration] request. Configuration JSON validation failed. Schema [{}], source={}, version [{}]", e.getSchemaUrl(), e.getSource(), e.getVersion());
			throw e;
		}
		
		String url = configuratorUrlService.createConfigurationUrl(config, "1.0");
		VpConfigurationRef ref = new VpConfigurationRef();
		ref.setConfigurationId(config.getFriendlyId());
		ref.setConfigurationUrl(url);
		httpResponse.addHeader("Location", url);
		httpResponse.addHeader("Cache-Control", "max-age=0, no-cache, no-store");
		httpResponse.addHeader("Pragma", "no-cache");
		httpResponse.addHeader("Expires", "-1");
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [PostConfiguration]. Assigned friendly ID [{}] with source [{}] in version [{}] in [{}] ms.", config.getFriendlyId(), config.getSource(), config.getVersion(), stopTime-startTime);
		logger.trace("[PostConfiguration] response: {}", ref);
		return new ResponseEntity<>(ref, HttpStatus.CREATED);		
	}
	
	private void validateInputVpConfig(VpConfiguration vpConfig) throws InvalidConfigurationContentException {
		List<String> invalidFields = new ArrayList<>();
		if (StringUtils.isEmpty(vpConfig.getConfigurationSource()))
			invalidFields.add("configurationSource");
		if (StringUtils.isEmpty(vpConfig.getVersion()))
			invalidFields.add("version");
		if (vpConfig.getConfigurationData() == null) {
			invalidFields.add("configurationData");
		} else {
			if (StringUtils.isEmpty(vpConfig.getConfigurationData().getContent()))
				invalidFields.add("configurationData.content");
			if (StringUtils.isEmpty(vpConfig.getConfigurationData().getShoppingBagSectionList())) {
				invalidFields.add("configurationData.shoppingBagSectionList");
			} else {
				String sbSlDecoded = new String(Base64.getDecoder().decode(vpConfig.getConfigurationData().getShoppingBagSectionList()), StandardCharsets.UTF_8);
				try {
					JaxbHelper.unmarshall(ShoppingBagSectionListComplexType.class, sbSlDecoded);
				} catch (JAXBException | IOException | ParserConfigurationException | SAXException e) {
					invalidFields.add("content.shoppingBagSectionList");
				}
			}
		}
		
		if (invalidFields.size() > 0) {
			throw new InvalidConfigurationContentException(invalidFields);
		}
	}

	@RequestMapping(value="/{configurationId:[a-zA-Z0-9]+}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<VpConfiguration> get(@PathVariable String configurationId, HttpServletResponse httpResponse) {
		long startTime = System.currentTimeMillis();
		logger.debug("GetConfiguration input configurationId={}", configurationId);
		
		Configuration config = configService.getConfigurationByFriendlyId(configurationId);
		if(config == null){
			logger.info("Unsuccessful [GetConfiguration] using friendly ID [{}] - not found", configurationId);
			throw new ResourceNotFoundException(); 
		}

		VpConfiguration vpConfig = ModelMapper.toVpConfiguration(config);
		httpResponse.addHeader("Cache-Control", "public");
		httpResponse.addHeader("Pragma", "cache");
		httpResponse.addDateHeader("Expires", System.currentTimeMillis()+props.getGetConfigurationTTLMinutes()*60000L);
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [GetConfiguration] of [{}] config using friendly ID [{}] in [{}] ms.", config.getSource(), config.getFriendlyId(), stopTime-startTime);
		logger.trace("[GetConfiguration] response body: {}", vpConfig);
		return new ResponseEntity<>(vpConfig, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/{configurationId:[a-zA-Z0-9]+}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable String ru, @PathVariable String lc, @PathVariable String configurationId) {
		long startTime = System.currentTimeMillis();
		logger.debug("DeleteConfiguration input ru={}, lc={}, configurationId={}", ru, lc, configurationId);
		
		try {
			configService.delete(configurationId, ru, lc);
		} catch (Exception e) {
			logger.info("Unsuccessful [DeleteConfiguration] using friendly ID [{}] (not found)", configurationId);
		}
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [DeleteConfiguration] using friendly ID [{}] in [{}] ms.", configurationId, stopTime-startTime);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
