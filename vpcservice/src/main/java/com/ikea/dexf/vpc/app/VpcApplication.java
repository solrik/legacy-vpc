package com.ikea.dexf.vpc.app;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.ikea.dexf.vpc", "se.ikea.addonapps.vpcservice"})
public class VpcApplication {


}
