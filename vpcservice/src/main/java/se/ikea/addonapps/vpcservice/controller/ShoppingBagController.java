package se.ikea.addonapps.vpcservice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

import msc.selling.common.shoppingbag._2.FindShoppingBagCriteriaComplexType;
import msc.selling.common.shoppingbag._2.GetShoppingBagCriteriaComplexType;
import msc.selling.common.shoppingbag._2.GetShoppingBagCriteriaListComplexType;
import msc.selling.common.shoppingbag._2.ObjectFactory;
import msc.selling.common.shoppingbag._2.ShoppingBagComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagListComplexType;
import se.ikea.addonapps.vpc.common.exception.UnexpectedException;
import se.ikea.addonapps.vpcservice.config.AppPropertiesVpcService;
import se.ikea.addonapps.vpcservice.exception.FileStorageException;
import se.ikea.addonapps.vpcservice.exception.ResourceNotFoundException;
import se.ikea.addonapps.vpcservice.model.Configuration;
import se.ikea.addonapps.vpcservice.service.ConfigurationService;
import se.ikea.addonapps.vpcservice.service.UrlService;
import se.ikea.addonapps.vpcservice.util.JaxbHelper;

/**
 * Invoked by IIP
 */
@Controller
@RequestMapping(value = {"/retail/vpc/configuration/shoppingbag", "/retail/{countryCode}/{languageCode}/vpc/configuration/shoppingbag"})
public class ShoppingBagController {

	@Autowired
	private ConfigurationService configService;
	
	@Autowired
	private UrlService urlService;
	
	@Autowired
	private AppPropertiesVpcService appProps;
	
	private static final Logger logger = LoggerFactory.getLogger(ShoppingBagController.class);

	@RequestMapping(value = "/{configurationIds}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public ResponseEntity<?> get(@PathVariable Optional<String> countryCode, @PathVariable Optional<String> languageCode, @PathVariable String[] configurationIds, HttpServletResponse httpResponse) throws FileStorageException {
		
		long startTime = System.currentTimeMillis();
		logger.debug("GetShoppingBag input, configIds={}", (Object[])configurationIds);
		
		if(configurationIds == null) {
			logger.warn("GetShoppingBag: No BagIds specified in request, returning 400");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		ObjectFactory objectFactory = new ObjectFactory();
		ShoppingBagListComplexType shoppingBagListComplexType = objectFactory.createShoppingBagListComplexType();

		for (String configId : configurationIds) {
			// check config is in DB and if its valid fetch the configuration
			Configuration config = null;
			try {
				config = configService.getConfigurationByFriendlyId(configId);
			} catch (FileStorageException e) {
				logger.warn("GetShoppingBag: Unable to read file from storage for config ID [{}]", configId, e);
			}
			if (config == null) {
				logger.warn("GetShoppingBag: BagId [{}] not found", configId);
				throw new ResourceNotFoundException();
			}
			
			String cc, lc = null;
			if (!countryCode.isPresent() || !languageCode.isPresent()) {
				logger.debug("GetShoppingBagCriteria: CountryCode and/or LanguageCode is empty. Will use original for ID [{}]: {}/{}", config.getFriendlyId(), config.getRetailUnit(), config.getLangCode());
				cc = config.getRetailUnit();
				lc = config.getLangCode();
			} else {
				cc = countryCode.get();
				lc = languageCode.get();
			}

			// construct response content
			String deepLinkUrl = urlService.createConfiguratorDeepLink(config, cc, lc);
			String shortUrl = urlService.createShortUrl(config, cc, lc);
			String bagUrl = urlService.createShoppingBagUrl(configId, cc, lc);
			ShoppingBagComplexType shoppingBagComplexType;
			try {
				shoppingBagComplexType = JaxbHelper.generateShoppingBag(config, bagUrl, deepLinkUrl, shortUrl);
			} catch (JAXBException | IOException | ParserConfigurationException | SAXException e) {
				logger.error("Unable to compose ShoppingBag for config ID [{}]", configId, e);
				throw new UnexpectedException(e);
			}
			shoppingBagListComplexType.getShoppingBag().add(shoppingBagComplexType);
		}

		String xmlResponse = null;
		try {
			xmlResponse = JaxbHelper.marshallShoppingBagService(shoppingBagListComplexType);
		} catch (JAXBException e) {
			logger.error("GetShoppingBag: Unable to marshall ShoppingBag", e);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		appendCacheHeaders(httpResponse);
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [GetShoppingBag] with BagId(s) [{}]  in [{}] ms.", configurationIds,  stopTime-startTime);
		logger.trace("[GetShoppingBag] response body: {}", xmlResponse);
		
		return new ResponseEntity<String>(xmlResponse, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/getcriteria", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public ResponseEntity<?> postGetCriteria(@RequestBody String payload, HttpServletResponse httpResponse) throws FileStorageException {
		long startTime = System.currentTimeMillis();
		logger.debug("POST GetShoppingBagCriteria input, payload={}", payload);
		
		GetShoppingBagCriteriaListComplexType criteriaList = null;
		try {
			criteriaList = JaxbHelper.getGetShoppingBagCriteriaList(payload);
		} catch (JAXBException e) {
			logger.warn("GetShoppingBag: Unable to parse request body: {}", payload, e);
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		if (criteriaList == null || criteriaList.getGetShoppingBagCriteria() == null || criteriaList.getGetShoppingBagCriteria().size() == 0) {
			logger.warn("GetShoppingBagCriteria: No criteria specified. Returning 404.");
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		
		List<ShoppingBagComplexType> foundBagsList = new ArrayList<>();
		StringBuilder bagIdsStr = new StringBuilder();
		for (GetShoppingBagCriteriaComplexType criteria : criteriaList.getGetShoppingBagCriteria()) {
			if (criteria.getBagId() == null || criteria.getBagId().trim().length() == 0) {
				logger.warn("GetShoppingBagCriteria: No BagId specified in criteria. Ignoring.");
				continue;
			}
			bagIdsStr.append(criteria.getBagId()).append(" ");
			Configuration config = configService.getConfigurationByFriendlyId(criteria.getBagId());
			if (config == null) {
				logger.info("GetShoppingBag: BagId [{}] not found", criteria.getBagId());
			} else {
				String countryCode = criteria.getCountryCode();
				String langCode = criteria.getLanguageCode();
				
				if (isEmpty(countryCode)) {
					logger.info("GetShoppingBagCriteria: CountryCode is empty. Will use original locale for ID [{}]: {}/{}", config.getFriendlyId(), config.getRetailUnit(), config.getLangCode());
					countryCode = config.getRetailUnit();
					langCode = config.getLangCode();
				} 
				countryCode = countryCode.toLowerCase();
				langCode = langCode.toLowerCase();
				
				String bagUrl = urlService.createShoppingBagUrl(config.getFriendlyId(), countryCode, langCode);
				String configDeepLinkUrl = urlService.createConfiguratorDeepLink(config, countryCode, langCode);
				String shortUrl = urlService.createShortUrl(config, countryCode, langCode);
				ShoppingBagComplexType sb;
				try {
					sb = JaxbHelper.generateShoppingBag(config, bagUrl, configDeepLinkUrl, shortUrl);
				} catch (JAXBException | IOException | ParserConfigurationException | SAXException e) {
					logger.error("Unable to compose ShoppingBag for config ID [{}]", criteria.getBagId(), e);
					throw new UnexpectedException(e);
				}
				foundBagsList.add(sb);
			}
		}
		
		if (foundBagsList.size() == 0) {
			logger.info("GetShoppingBag: No ShoppingBags matching the get-criteria were found. Throwing 404");
			throw new ResourceNotFoundException();
		}
		ShoppingBagListComplexType bagList = JaxbHelper.generateShoppingBagList(foundBagsList);
		String xmlResponse = null;
		try {
			xmlResponse = JaxbHelper.marshallShoppingBagService(bagList);
		} catch (JAXBException e) {
			logger.error("POST GetShoppingBagCriteria: Unable to marshall ShoppingBag", e);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		appendCacheHeaders(httpResponse);
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [POST GetShoppingBagCriteria] with BagId(s) [{}]  in [{}] ms.", bagIdsStr.toString(),  stopTime-startTime);
		logger.trace("[POST GetShoppingBagCriteria] response body: {}", xmlResponse);
		
		return new ResponseEntity<String>(xmlResponse, HttpStatus.OK);
	}

	private boolean isEmpty(String s) {
		return (s == null || s.trim().length() == 0);
	}

	@RequestMapping(value = "/findcriteria", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public ResponseEntity<?> postFindCriteria(@RequestBody String payload, HttpServletResponse httpResponse) throws FileStorageException {
		long startTime = System.currentTimeMillis();
		logger.debug("FindShoppingBag input, payload={}", payload);

		FindShoppingBagCriteriaComplexType findShoppingBagCriteriaComplexType = null;
		try {
			findShoppingBagCriteriaComplexType = JaxbHelper.getFindShoppingBagCriteria(payload);
		} catch (JAXBException e) {
			logger.warn("FindShoppingBag: Unable to parse request body: {}", payload, e);
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		if (findShoppingBagCriteriaComplexType == null || findShoppingBagCriteriaComplexType.getBagId() == null) {
			logger.warn("FindShoppingBag: No BagId specified. Returning 404.");
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		Configuration config = configService.getConfigurationHead(findShoppingBagCriteriaComplexType.getBagId());
		// if configuration not found return 404
		if (config == null) {
			logger.info("ShoppingBag for configuration [{}] was not found. Returning 404.", findShoppingBagCriteriaComplexType.getBagId());
			throw new ResourceNotFoundException();
		}
		
		String countryCode = findShoppingBagCriteriaComplexType.getCountryCode();
		String langCode = findShoppingBagCriteriaComplexType.getLanguageCode();
		if (isEmpty(countryCode) || isEmpty(langCode)) {
			logger.debug("FindShoppingBag: CountryCode and/or LanguageCode is empty. Will use original for ID [{}]: {}/{}", config.getFriendlyId(), config.getRetailUnit(), config.getLangCode());
			countryCode = config.getRetailUnit();
			langCode = config.getLangCode();
		}
		
		ShoppingBagListComplexType shoppingBagListComplexType = JaxbHelper.generateShoppingBagHeadInList(config, urlService.createShoppingBagUrl(config.getFriendlyId(), countryCode, langCode));
		String xmlResponse = null;
		try {
			xmlResponse = JaxbHelper.marshallShoppingBagService(shoppingBagListComplexType);
		} catch (JAXBException e) {
			logger.error("Unable to marshall ShoppingBag", e);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		httpResponse.addHeader("Cache-Control", "max-age=0, no-cache, no-store");
		httpResponse.addHeader("Pragma", "no-cache");
		httpResponse.addHeader("Expires", "-1");
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [FindShoppingBag] with BagId [{}] in [{}] ms.", config.getFriendlyId(), stopTime-startTime);
		logger.trace("[FindShoppingBag] response body: {}", xmlResponse);

		return new ResponseEntity<String>(xmlResponse, HttpStatus.OK);
	}
	
	private void appendCacheHeaders(HttpServletResponse httpResponse) {
		httpResponse.addHeader("Cache-Control", "public");
		httpResponse.addHeader("Pragma", "cache");
		httpResponse.addDateHeader("Expires", System.currentTimeMillis()+appProps.getGetConfigurationTTLMinutes()*60000L);
	}
}
