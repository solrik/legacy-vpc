package se.ikea.addonapps.vpcservice.config;

import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import se.ikea.addonapps.vpc.common.config.AppProperties;
import se.ikea.addonapps.vpcservice.exception.ResourceNotFoundException;

@Configuration
@ComponentScan("se.ikea.addonapps.vpcservice")
public class AppPropertiesVpcService extends AppProperties {

	private PropertyResourceBundle schemaProps;
	private PropertyResourceBundle localeProps;
	private static final int DEFAULT_STORAGE_SERVICE_POOL_SIZE = 7;
	private static final long DEFAULT_STORAGE_SERVICE_POOL_MAXWAIT_MS = 10000L;
	private static final String DEFAULT_STORAGE_SERVICE_VERSION = "1.0";
	private static final long DEFAULT_CACHE_TTL_GETCONFIGURATION_MINUTES = 10080L;
	
	private static final Logger logger = LoggerFactory.getLogger(AppPropertiesVpcService.class);

	private static final String PROP_DB_PASSWORD = "db_password";

	private static final String PROP_STORAGESERVICE_USER = "storageservice_user";
	private static final String PROP_STORAGESERVICE_PASSWORD = "storageservice_password";
	private static final String PROP_STORAGESERVICE_POOL_MAXWAITMS = "storageservice_pool_maxwaitms";
	private static final String PROP_STORAGESERVICE_POOL_SIZE = "storageservice_pool_size";
	private static final String PROP_STORAGESERVICE_VERSION = "storageservice_version";
	private static final String PROP_STORAGESERVICE_CONSUMERID = "storageservice_consumerid";
	private static final String PROP_STORAGESERVICE_CONTRACTID = "storageservice_contractid";
	private static final String PROP_STORAGESERVICE_URL_DELETE = "storageservice_url_delete";
	private static final String PROP_STORAGESERVICE_URL_GET = "storageservice_url_get";
	private static final String PROP_STORAGESERVICE_URL_POST = "storageservice_url_post";
	private static final String PROP_CACHE_TTL_GETCONFIGURATION_MINUTES = "cache_ttl_getconfiguration_minutes";

	private static final String PROP_JSON_SCHEMA_LOCATION_URL = "json_schema_location_url";
	private static final String PROP_PLANNER_DEEPLINK_URL_PATTERN_PREFIX = "planner_deeplink_url_pattern_";
	private static final String PROP_PLANNER_DEEPLINK_URL_PATTERN_GLOBAL = PROP_PLANNER_DEEPLINK_URL_PATTERN_PREFIX+"global";
	private static final String PROP_PLANNER_SHORT_URL_PATTERN_PREFIX = "planner_short_url_pattern_";
	private static final String PROP_PLANNER_SHORT_URL_PATTERN_GLOBAL = PROP_PLANNER_SHORT_URL_PATTERN_PREFIX+"global";

	private static final String PROP_BACKOFFICE_SERVICE_HOST_NAME = "backofficeservice_hostname";
	private static final String PROP_BACKOFFICE_SERVICE_PORT = "backofficeservice_port";
	private static final String PROP_SCHEDULER_SERVICE_HOST_NAME = "schedulerservice_hostname";
	private static final String PROP_SCHEDULER_SERVICE_PORT = "schedulerservice_port";
	private static final String PROP_SCH_CATEGORY_SHARE_SAVE_EMAIL = "sch_category_share_save_email";
	
	public AppPropertiesVpcService() {
		super();
		try {
			schemaProps = (PropertyResourceBundle)PropertyResourceBundle.getBundle("schemas_"+getEnv());
			localeProps = (PropertyResourceBundle)PropertyResourceBundle.getBundle("locales");
		} catch (ResourceNotFoundException e) {
			alert("Unable to find property file for env variable [{}]", e, getEnv());
		}
	}

	public String getStorageServicePostUrl() {
		return getProperty(PROP_STORAGESERVICE_URL_POST);
	}
	
	public String getStorageServiceGetUrl() {
		return getProperty(PROP_STORAGESERVICE_URL_GET);
	}
	
	public String getStorageServiceDeleteUrl() {
		return getProperty(PROP_STORAGESERVICE_URL_DELETE);
	}
	
	public String getStorageServiceContractId() {
		return getProperty(PROP_STORAGESERVICE_CONTRACTID);
	}
	
	public String getStorageServiceConsumerId() {
		return getProperty(PROP_STORAGESERVICE_CONSUMERID);
	}
	
	public String getStorageServiceVersion() {
		String version = getProperty(PROP_STORAGESERVICE_VERSION);
		if (version != null) {
			return version;
		}
		return DEFAULT_STORAGE_SERVICE_VERSION;
	}
	
	public int getStorageServicePoolMaxTotalConnections() {
		String poolSize = getProperty(PROP_STORAGESERVICE_POOL_SIZE);
		if (poolSize != null) {
			return Integer.parseInt(poolSize);
		}
		return DEFAULT_STORAGE_SERVICE_POOL_SIZE;
	}

	public long getStorageServicePoolMaxWaitMillis() {
		String maxwait = getProperty(PROP_STORAGESERVICE_POOL_MAXWAITMS);
		if (maxwait != null) {
			return Integer.parseInt(maxwait);
		}
		return DEFAULT_STORAGE_SERVICE_POOL_MAXWAIT_MS;
	}
	
	public long getGetConfigurationTTLMinutes() {
		String s = getProperty(PROP_CACHE_TTL_GETCONFIGURATION_MINUTES);
		if (s != null) {
			return Long.parseLong(s);
		}
		return DEFAULT_CACHE_TTL_GETCONFIGURATION_MINUTES;
	}

	public String getStorageServicePwd() {
		return getProperty(PROP_STORAGESERVICE_PASSWORD);
	}

	public String getStorageServiceUid() {
		return getProperty(PROP_STORAGESERVICE_USER);
	}
	
	/**
	 * Property names in priority order:
	 * 1. planner_deeplink_url_pattern_<plannner>_<ru>
	 * 2. planner_deeplink_url_pattern_<planner>
	 * 3. planner_deeplink_url_pattern_<ru>
	 * 4. planner_deeplink_url_pattern_global
	 * 
	 * @param source
	 * @param ru
	 * @return
	 */
	public String getUrlPatternDeeplink(String source, String ru) {
		String pattern = getProperty(PROP_PLANNER_DEEPLINK_URL_PATTERN_PREFIX+source+"_"+ru);
		if (pattern != null)
			return pattern;
		else if ((pattern=getProperty(PROP_PLANNER_DEEPLINK_URL_PATTERN_PREFIX+source)) != null)
			return pattern;
		else if ((pattern=getProperty(PROP_PLANNER_DEEPLINK_URL_PATTERN_PREFIX+ru)) != null)
			return pattern;
		
		return getProperty(PROP_PLANNER_DEEPLINK_URL_PATTERN_GLOBAL);
	}
	
	/**
	 * Property names in priority order:
	 * 1. planner_short_url_pattern_<planner>_<ru>
	 * 2. planner_short_url_pattern_<planner>
	 * 3. planner_short_url_pattern_<ru>
	 * 4. planner_short_url_pattern_global
	 * 
	 * @param source
	 * @param ru
	 * @return
	 */
	public String getUrlPatternShortUrl(String source, String ru) {
		String pattern = getProperty(PROP_PLANNER_SHORT_URL_PATTERN_PREFIX+source+"_"+ru);
		if (pattern != null)
			return pattern;
		else if ((pattern=getProperty(PROP_PLANNER_SHORT_URL_PATTERN_PREFIX+source)) != null)
			return pattern;
		else if ((pattern=getProperty(PROP_PLANNER_SHORT_URL_PATTERN_PREFIX+ru)) != null)
			return pattern;
		
		return getProperty(PROP_PLANNER_SHORT_URL_PATTERN_GLOBAL);
	}
	
	public String getBackofficeServiceHostName() {
		return getProperty(PROP_BACKOFFICE_SERVICE_HOST_NAME);
	}
	
	public String getBackofficeServicePort() {
		return getProperty(PROP_BACKOFFICE_SERVICE_PORT);
	}
	
	public String getSchedulerServiceHostName() {
		return getProperty(PROP_SCHEDULER_SERVICE_HOST_NAME);
	}
	
	public String getSchedulerServicePort() {
		return getProperty(PROP_SCHEDULER_SERVICE_PORT);
	}
	
	public String getDbPassword() {
		return getProperty(PROP_DB_PASSWORD);
	}
	
	public String getJsonSchemaLocationUrl() {
		return getProperty(PROP_JSON_SCHEMA_LOCATION_URL);
	}

	public String getSchCategoryShareSaveEmail() {
		return getProperty(PROP_SCH_CATEGORY_SHARE_SAVE_EMAIL);
	}

	public String getSourceSchemaFileNameByNameAndVersion(String name, String version) {
		try {
			return schemaProps.getString(name+"_"+version);
		} catch (MissingResourceException e) {
			logger.warn("Failed to find JSON schema for source [{}] in version [{}]", name, version);
			return null;
		}
	}

	/**
	 * Returns a list of supported language codes for the specified country codes. The first code (at position 0) is default.
	 * @param countryCode
	 * @return
	 */
	public String[] getSupportedLanguageCodes(String countryCode) {
		String propValue = null;
		try {
			propValue = localeProps.getString(countryCode);
		} catch (MissingResourceException e) {
			propValue = null;
		}
		if (propValue == null)
			return null;
		return propValue.split(",");
	}

}
