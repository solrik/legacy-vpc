package se.ikea.addonapps.vpc.common.util;

public class RestHelper {

	public static final String SCHEDULER_POST_JOB_URL_FORMAT = "http://%s:%s/vpcservice-scheduler/job";
	
	// http://host:9000/vpcservice-backoffice/se/sv/share/email?template=test&configuration=123ABC&addresses=test%40ikea.com;
	public static final String SHARE_EMAIL_POST_URL = "http://%s:%s/vpcservice-backoffice/%s/%s/share/email";
	
	public static final String SHARE_EMAIL_POST_PARAMS_FORMAT = "template=%s&configurationId=%s&storeBuCode=%s&addresses=%s";
}
