package se.ikea.addonapps.vpcservice.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;

public class SchemaCacheEventListener implements CacheEventListener {

	private static final Logger logger = LoggerFactory.getLogger(SchemaCacheEventListener.class);
	
	@Override
	public void notifyElementRemoved(Ehcache cache, Element element) throws CacheException {
		logger.debug("Cache entry removed from cache [{}]: [{}]", cache.getName(), element.getObjectKey());
	}

	@Override
	public void notifyElementPut(Ehcache cache, Element element) throws CacheException {
		logger.debug("Cache entry added to cache [{}]: [{}]", cache.getName(), element.getObjectKey());
	}

	@Override
	public void notifyElementUpdated(Ehcache cache, Element element) throws CacheException {
		logger.debug("Cache entry updated in cache [{}]: [{}]", cache.getName(), element.getObjectKey());
	}

	@Override
	public void notifyElementExpired(Ehcache cache, Element element) {
		logger.debug("Cache entry expired in cache [{}]: [{}]", cache.getName(), element.getObjectKey());

	}

	@Override
	public void notifyElementEvicted(Ehcache cache, Element element) {
		logger.debug("Cache entry evicted from cache [{}]: [{}]", cache.getName(), element.getObjectKey());
	}

	@Override
	public void notifyRemoveAll(Ehcache cache) {
		logger.info("Cache purged: [{}]", cache.getName());
	}

	@Override
	public void dispose() {
		logger.debug("I will be disposed");
	}
	
	public Object clone() throws CloneNotSupportedException {
		return this.clone();
	}

}
