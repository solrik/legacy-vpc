package se.ikea.addonapps.vpc.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.ikea.addonapps.vpc.common.exception.UnexpectedException;

public class JsonHelper {

	private static final Logger logger = LoggerFactory.getLogger(JsonHelper.class);
	
	private JsonHelper() {}
	
	public static String marshalToJson(Object o, boolean pretty) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			if (pretty)
				json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
			else
				json = mapper.writeValueAsString(o);
			logger.debug("Marshalled object of type [{}] to JSON: {}", o.getClass().getName(), json);
			return json;
		} catch (JsonProcessingException e1) {
			throw new UnexpectedException("Failed to marshal object of type "+o.getClass().getName()+" to JSON", e1);
		}
	}
}
