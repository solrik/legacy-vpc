package se.ikea.addonapps.vpc.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlertHelper {

	private AlertHelper(){}
	
	public static final Logger CONFIG_ALERT_LOGGER = LoggerFactory.getLogger("alert.CONFIG");
	public static final Logger FILE_STORAGE_ALERT_LOGGER = LoggerFactory.getLogger("alert.STORAGESERVICE");
	public static final Logger DB_ALERT_LOGGER = LoggerFactory.getLogger("alert.DB");
	public static final Logger ASYNC_JOB_ERROR_ALERT_LOGGER = LoggerFactory.getLogger("alert.ASYNC");
	public static final Logger UNEXPECTED_ERROR_ALERT_LOGGER = LoggerFactory.getLogger("alert.UNKNOWN");
	public static final Logger RETAIL_ITEM_COMMUNICATION_ALERT_LOGGER = LoggerFactory.getLogger("alert.RICSERVICE");
	public static final Logger CONTENT_SERVICE_ALERT_LOGGER = LoggerFactory.getLogger("alert.TRANSSERVICE");
	public static final Logger RESOURCE_ALERT_LOGGER = LoggerFactory.getLogger("alert.RESOURCE");
	public static final Logger BO_ALERT_LOGGER = LoggerFactory.getLogger("alert.BACKOFFICE");
	public static final Logger PLANNER_CLIENT_ALERT_LOGGER = LoggerFactory.getLogger("alert.PLANNERCLIENT");
	public static final Logger STOCK_AVAILABILITY_ALERT_LOGGER = LoggerFactory.getLogger("alert.STOCKAVAILABILITY");
	public static final Logger STORE_SERVICE_ALERT_LOGGER = LoggerFactory.getLogger("alert.STORESERVICE");
	
}
