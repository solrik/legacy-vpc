package se.ikea.addonapps.vpcservice.jpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.ikea.addonapps.vpc.common.exception.DbException;
import se.ikea.addonapps.vpcservice.jpa.repository.VPCConfigurationRepository;
import se.ikea.addonapps.vpcservice.model.Configuration;

/**
 * @author shashi
 *
 */
@Service
public class VPCConfigurationPersistenceService {

	@Autowired
	private VPCConfigurationRepository repository;


	public void persist(Configuration config) throws DbException {
		try {
			repository.save(config);
		} catch (Exception e) {
			throw new DbException(e);
		}
	}
	
	public Configuration findById(long friendlyId) throws DbException{
		try {
			return repository.findById(friendlyId);
		} catch (Exception e) {
			throw new DbException(e);
		}
	}
	
	public Configuration findByHash(String hash) throws DbException{
		try {
			return repository.findByHash(hash);
		} catch (Exception e) {
			throw new DbException(e);
		}
	}

	public void deleteById(long id) throws DbException {
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			throw new DbException(e);
		}
	}
	
	public Long countConfigurations() throws DbException {
		try {
			return repository.countConfigurations();
		} catch (Exception e) {
			throw new DbException(e);
		}		
	}

	public Configuration getLatestConfiguration() throws DbException {
		try {
			return repository.findTop1ByOrderByIdDesc();
		} catch (Exception e) {
			throw new DbException(e);
		}
	}

	

}
