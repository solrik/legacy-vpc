package se.ikea.addonapps.vpc.common.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonRootName(value = "attachment")
public class Attachment {

	private String id;
	private String data;
	private String mimeType;
	private boolean inline;
	
	public boolean isInline() {
		return inline;
	}

	public void setInline(boolean inline) {
		this.inline = inline;
	}

	@JsonGetter
	public String getId() {
		return id;
	}
	
	@JsonSetter
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonGetter
	public String getData() {
		return data;
	}
	
	@JsonSetter
	public void setData(String data) {
		this.data = data;
	}

	@JsonGetter
	public String getMimeType() {
		return mimeType;
	}

	@JsonSetter
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	

}
