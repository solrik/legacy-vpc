<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>se.ikea.addonapps</groupId>
	<artifactId>vpcservice-swagger-jaxrs</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<build>

		<!-- activate the plugin -->
		<plugins>
			<plugin>
				<groupId>io.swagger</groupId>
				<artifactId>swagger-codegen-maven-plugin</artifactId>
				<version>2.2.3</version>
				<executions>
					<execution>
						<id>generate-planneritemlist</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<!-- specify the swagger yaml -->
							<inputSpec>src/main/resources/planneritemlist-1.0.1-swagger.yaml</inputSpec>

							<!-- target to generate java client code -->
							<language>java</language>
							<invokerPackage>io.swagger</invokerPackage>
                            <apiPackage>io.swagger.api</apiPackage>
                            <modelPackage>io.swagger.pil.model</modelPackage>
							<!-- apiPackage>se.ikea.iopg.gen</apiPackage -->
							<!-- hint: if you want to generate java server code, e.g. based on 
								Spring Boot, you can use the following target: <language>spring</language> -->
							<language>jaxrs</language>
							<!-- pass any necessary config options -->
							<configOptions>
								<dateLibrary>java8</dateLibrary>
							</configOptions>
							<configOptions>
								<sourceFolder>src/main/java</sourceFolder>
							</configOptions>
							<output>target</output>
							<!-- override the default library to jersey2 -->
							<library>jersey2</library>
							<templateDirectory>src/main/resources/templates</templateDirectory>
						</configuration>
					</execution>
					<execution>
						<id>generate-vpcservice</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<!-- specify the swagger yaml -->
							<inputSpec>src/main/resources/vpcservice-1.0.0-swagger.yaml</inputSpec>

							<!-- target to generate java client code -->
							<language>java</language>
							<invokerPackage>io.swagger</invokerPackage>
                            <apiPackage>io.swagger.api</apiPackage>
                            <modelPackage>io.swagger.vpcservice.model</modelPackage>
							<!-- hint: if you want to generate java server code, e.g. based on 
								Spring Boot, you can use the following target: <language>spring</language> -->
							<language>jaxrs</language>
							<!-- pass any necessary config options -->
							<configOptions>
								<dateLibrary>java8</dateLibrary>
							</configOptions>
							<configOptions>
								<sourceFolder>src/main/java</sourceFolder>
							</configOptions>
							<output>target</output>
							<!-- override the default library to jersey2 -->
							<library>jersey2</library>
							<templateDirectory>src/main/resources/templates</templateDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.6.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<encoding>${project.build.sourceEncoding}</encoding>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings only. It has no influence on the Maven build itself.-->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>io.swagger</groupId>
										<artifactId>
											swagger-codegen-maven-plugin
										</artifactId>
										<versionRange>
											[2.2.2,)
										</versionRange>
										<goals>
											<goal>generate</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	<dependencies>
		<!-- dependencies are needed for the client being generated -->

		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>${swagger-annotations-version}</version>
		</dependency>

		<!-- You can find the dependencies for the library configuation you chose 
			by looking in JavaClientCodegen. Then find the corresponding dependency on 
			Maven Central, and set the versions in the property section below -->

		<!-- HTTP client: jersey-client -->
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-client</artifactId>
			<version>${jersey-version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-json-jackson</artifactId>
			<version>${jersey-version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-multipart</artifactId>
			<version>${jersey-version}</version>
		</dependency>

		<!-- JSON processing: jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-base</artifactId>
			<version>${jackson-version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>${jackson-version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>${jackson-version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson-version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
			<version>${jackson-version}</version>
		</dependency>

		<!-- Joda time: if you use it -->
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-joda</artifactId>
			<version>${jackson-version}</version>
		</dependency>
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>${jodatime-version}</version>
		</dependency>

		<!-- Base64 encoding that works in both JVM and Android -->
		<dependency>
			<groupId>com.brsanthu</groupId>
			<artifactId>migbase64</artifactId>
			<version>2.2</version>
		</dependency>

		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-jaxrs</artifactId>
			<version>1.5.13</version>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
		</dependency>
	</dependencies>

	<properties>
		<swagger-annotations-version>1.5.13</swagger-annotations-version>
		<jersey-version>2.25.1</jersey-version>
		<jackson-version>2.9.4</jackson-version>
		<jodatime-version>2.7</jodatime-version>
		<maven-plugin-version>1.0.0</maven-plugin-version>
		<junit-version>4.8.1</junit-version>
	</properties>
</project>