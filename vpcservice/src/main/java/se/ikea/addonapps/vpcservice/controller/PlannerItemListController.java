package se.ikea.addonapps.vpcservice.controller;

import java.io.IOException;
import java.time.Instant;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

import io.swagger.pil.model.PlannerItemList;
import io.swagger.pil.model.PlannerItemListContent;
import io.swagger.pil.model.PlannerItemListSection;
import io.swagger.pil.model.PlannerProjectInformation;
import msc.selling.common.shoppingbag._2.ShoppingBagItemComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagSectionComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagSectionListComplexType;
import se.ikea.addonapps.vpc.common.exception.UnexpectedException;
import se.ikea.addonapps.vpcservice.exception.ResourceNotFoundException;
import se.ikea.addonapps.vpcservice.model.Configuration;
import se.ikea.addonapps.vpcservice.service.ConfigurationService;
import se.ikea.addonapps.vpcservice.service.UrlService;

/*
 * Invoked by INTER API GW
 */
@Controller
@RequestMapping(value = {"/retail/{retailUnit}/{langCode}/vpc/planneritemlist"})
public class PlannerItemListController {
	
	private static final Logger logger = LoggerFactory.getLogger(PlannerItemListController.class);
	
	@Autowired
	private ConfigurationService configService;
	
	@Autowired
	private UrlService urlService;

	@RequestMapping(value="/{configId}/store/{storeNo}", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@ResponseBody
	public ResponseEntity<PlannerItemList> getPlannerItemList(@PathVariable String retailUnit, @PathVariable String langCode, @PathVariable String configId, 
			@PathVariable String storeNo) {
		PlannerItemList itemList = new PlannerItemList();
		
		long startTime = System.currentTimeMillis();
		logger.debug("GetPlannerItemList input, configId={}, storeNo={}", configId, storeNo);

		// check config is in DB and if its valid fetch the configuration
		Configuration config = null;
		config = configService.getConfigurationByFriendlyId(configId);
		if (config == null) {
			logger.warn("GetPlannerItemList: BagId [{}] not found", configId);
			throw new ResourceNotFoundException();
		}
		
		itemList.setSchemaVersion(config.getVersion());
		itemList.setSourceSystem(config.getSource());
		itemList.setTimestamp(Instant.now().toString());
		
		PlannerProjectInformation projectInfo = new PlannerProjectInformation();
		projectInfo.setId(configId);
		projectInfo.setUrlInPlanner(urlService.createShortUrl(config, retailUnit, langCode));
		itemList.setProjectInformation(projectInfo);
		
		ShoppingBagSectionListComplexType sectionList;
		try {
			sectionList = config.getShoppingBagSectionListXml();
		} catch (JAXBException | IOException | ParserConfigurationException | SAXException e) {
			logger.error("Unable to unmarshal ShoppingBagSectionList for config ID [{}]", configId, e);
			throw new UnexpectedException(e);
		}
		int sectionId = 0;
		for (ShoppingBagSectionComplexType section : sectionList.getShoppingBagSection()) {
			PlannerItemListSection ilSection = new PlannerItemListSection();
			ilSection.sectionId(Integer.valueOf(++sectionId));
			
			for (ShoppingBagItemComplexType sbItem : section.getShoppingBagItemList().getShoppingBagItem()) {
				PlannerItemListContent itemContent = new PlannerItemListContent();
				itemContent.setItemNo(sbItem.getItemNo());
				itemContent.setItemType(sbItem.getItemType().value());
				itemContent.setItemQty(sbItem.getItemQty());
				ilSection.addContentItem(itemContent);
			}
			
			itemList.addSectionsItem(ilSection);
		}

		long stopTime = System.currentTimeMillis();
		logger.info("Successful [GetPlannerItemList] with configuration ID [{}]  in [{}] ms.", configId,  stopTime-startTime);
		logger.trace("[GetPlannerItemList] response body: {}", itemList);
		return new ResponseEntity<>(itemList, HttpStatus.OK);
	}
}
