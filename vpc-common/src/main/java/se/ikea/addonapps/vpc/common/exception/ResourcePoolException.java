package se.ikea.addonapps.vpc.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import se.ikea.addonapps.vpc.common.util.AlertHelper;

@ResponseStatus(value=HttpStatus.SERVICE_UNAVAILABLE)
public class ResourcePoolException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ResourcePoolException(Exception e, Class<?> resourceClass) {
		super(e);
		String msg = "Failed to allocate connection resource "+resourceClass.getName(); 
		AlertHelper.RESOURCE_ALERT_LOGGER.error(msg, e);
	}
}
