package se.ikea.addonapps.vpc.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import se.ikea.addonapps.vpc.common.exception.UnexpectedException;

public class Sha256Generator {

	/**
	 * @param input
	 * @return
	 */
	public static String getSha256(String input) {
		MessageDigest md = null;
		final StringBuilder sb = new StringBuilder();
		try {
			md = MessageDigest.getInstance("SHA-256");
			byte[] hash = md.digest(input.getBytes("UTF-8"));

			for (int i = 0; i < hash.length; i++) {
				sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (UnsupportedEncodingException|NoSuchAlgorithmException e) {
			throw new UnexpectedException("Unable to create SHA256 hash", e);
		}
	}
	
}
