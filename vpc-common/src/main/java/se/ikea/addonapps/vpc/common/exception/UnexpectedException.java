package se.ikea.addonapps.vpc.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import se.ikea.addonapps.vpc.common.util.AlertHelper;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class UnexpectedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnexpectedException(Exception e) {
		super(e);
		AlertHelper.UNEXPECTED_ERROR_ALERT_LOGGER.error("Unexpected error", e);
	}
	
	public UnexpectedException(String msg) {
		super(msg);
		AlertHelper.UNEXPECTED_ERROR_ALERT_LOGGER.error(msg);
	}
	
	public UnexpectedException(String msg, Exception e) {
		super(msg, e);
		AlertHelper.UNEXPECTED_ERROR_ALERT_LOGGER.error(msg, e);
	}
}
