package se.ikea.addonapps.vpcservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import se.ikea.addonapps.vpcservice.config.AppPropertiesVpcService;

@Service
public class JsonSchemaValidatorService {
	
	private static final Logger logger = LoggerFactory.getLogger(JsonSchemaValidatorService.class);
	public static final String JSON_V4_SCHEMA_IDENTIFIER = "http://json-schema.org/draft-04/schema#";
    public static final String JSON_SCHEMA_IDENTIFIER_ELEMENT = "$schema";
    
    @Autowired
    private AppPropertiesVpcService appProps;
    
    @Autowired
    private JsonSchemaProviderService schemaProvider;
    
    public boolean isJsonValid(String schemaFileName, String jsonText) {    	
		String urlBase = appProps.getJsonSchemaLocationUrl();
		if (!urlBase.endsWith("/"))
			urlBase = urlBase+"/";
		JsonNode schemaNode = schemaProvider.getSourceSchema(urlBase+schemaFileName);
		if (schemaNode == null) {
			logger.warn("Schema [{}] was not found", urlBase+schemaFileName);
			return false;
		}
		
    	final JsonNode schemaIdentifier = schemaNode.get(JSON_SCHEMA_IDENTIFIER_ELEMENT);
        if (null == schemaIdentifier){
            ((ObjectNode) schemaNode).put(JSON_SCHEMA_IDENTIFIER_ELEMENT, JSON_V4_SCHEMA_IDENTIFIER);
        }
             
        try {
	        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
	        JsonSchema schema = factory.getJsonSchema(schemaNode);
	        JsonNode configurationJson = JsonLoader.fromString(jsonText);
	        ProcessingReport report = schema.validate(configurationJson);
	        if (report.isSuccess()) {
	        	return true;
	        }
	        for (ProcessingMessage msg : report) {
	        	logger.warn("[{}]: Json validation error message: {}", schemaFileName, msg.getMessage());
	        }
	        return false;
        } catch (Exception pe) {
        	logger.error("[{}]: Unknown exception during JSON schema validation", schemaFileName, pe);
        	return false;
        }
    }
}
