package se.ikea.addonapps.vpc.common.util;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;

import se.ikea.addonapps.vpc.common.exception.InvalidContentException;
import se.ikea.addonapps.vpc.common.model.scheduler.Job;

public class ValidationHelper {

	private ValidationHelper() {}
	
	public static String validateEmail(String email) throws InvalidContentException {
		if (!EmailValidator.getInstance().isValid(email))
			throw new InvalidContentException("Invalid e-mail: ["+email+"]");
		return email;
	}
	
	public static String validateUrl(String url) throws InvalidContentException {
		if (!new UrlValidator(UrlValidator.ALLOW_LOCAL_URLS).isValid(url))
			throw new InvalidContentException("Invalid url: ["+url+"]");
		return url;
	}
	
	public static String validateHttpMethod(String method) throws InvalidContentException {
		if (
				Job.METHOD_DELETE.equalsIgnoreCase(method) || 
				Job.METHOD_GET.equalsIgnoreCase(method) || 
				Job.METHOD_POST.equalsIgnoreCase(method) || 
				Job.METHOD_PUT.equalsIgnoreCase(method)
			)
				return method;

		throw new InvalidContentException("Invalid HTTP method ["+method+"]");
	}
}
