Simply use File -> Open to Open the legacy-vpc folder in IntelliJ.  

Build all projects except vpc-service itself.  

After everything else is built, "Execute Maven Goal" with IntelliJ in the vpc-service project: "-Denv=dev clean install"  