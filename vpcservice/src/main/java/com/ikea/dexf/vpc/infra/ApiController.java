package com.ikea.dexf.vpc.infra;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    @GetMapping(value = "ping")
    public String ping() {
        return "pong";
    }
}
