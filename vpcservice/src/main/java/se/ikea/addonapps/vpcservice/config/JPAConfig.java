package se.ikea.addonapps.vpcservice.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.cfg.AvailableSettings;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("se.ikea.addonapps.vpcservice")
@EnableJpaRepositories(basePackages = {"se.ikea.addonapps.vpcservice.jpa.repository"})
@EnableTransactionManagement
public class JPAConfig {
	
	@Autowired
	private AppPropertiesVpcService appProperties;
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        transactionManager.setDataSource(dataSource());
        transactionManager.setJpaDialect(jpaDialect());
		return transactionManager;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		final LocalContainerEntityManagerFactoryBean lcmfb = new LocalContainerEntityManagerFactoryBean();
		lcmfb.setPersistenceProvider(new HibernatePersistenceProvider());
		lcmfb.setJpaProperties(jpaProperties());
		lcmfb.setJpaDialect(jpaDialect());
		lcmfb.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		lcmfb.setJpaVendorAdapter(jpaVendorAdapter());
		lcmfb.setPackagesToScan(new String[] {"se.ikea.addonapps.vpcservice"});
		lcmfb.setDataSource(dataSource());
		return lcmfb;
	}
	
	@Bean
	public Properties jpaProperties(){
		final Properties jpaProperties = new Properties();
		jpaProperties.setProperty(AvailableSettings.DIALECT, appProperties.getDbJpaHibernateDialect());
		jpaProperties.setProperty(AvailableSettings.HBM2DDL_AUTO, appProperties.getDbJpaHibernateHb22DdlAuto());
		jpaProperties.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
		
		return jpaProperties;
	}
	
	@Bean
	public JpaDialect jpaDialect() {
		return new HibernateJpaDialect();
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		hibernateJpaVendorAdapter.setShowSql(false);
		hibernateJpaVendorAdapter.setGenerateDdl(true);
		hibernateJpaVendorAdapter.setDatabase(Database.valueOf(appProperties.getDbVendor()));
		return hibernateJpaVendorAdapter;
	}

	@Bean
	public DataSource dataSource() {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(appProperties.getDbJdbcDriverClassName());
		dataSource.setUrl(appProperties.getDbJdbcUrl());
		dataSource.setUsername(appProperties.getDbUserName());
		dataSource.setPassword(appProperties.getDbPassword());

		return dataSource;
	}
	

}
