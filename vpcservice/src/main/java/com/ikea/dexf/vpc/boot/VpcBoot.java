package com.ikea.dexf.vpc.boot;

import com.ikea.dexf.vpc.app.VpcApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VpcBoot {

    public static void main(String[] args) {

        SpringApplication.run(VpcApplication.class, args);
    }
}
