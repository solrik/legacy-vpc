package se.ikea.addonapps.vpcservice.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;

@Service
public class JsonSchemaProviderService implements ResourceLoaderAware {
	
	private static final Logger logger = LoggerFactory.getLogger(JsonSchemaProviderService.class);
	private ResourceLoader resourceLoader;
	
	@Cacheable(value = "sourceschemas", unless="#result == null")
	public JsonNode getSourceSchema(String schemaUrl) {
		logger.debug("Looking up schema [{}]", schemaUrl);
		Resource r = resourceLoader.getResource(schemaUrl);
		try {
			return JsonLoader.fromFile(r.getFile());
		} catch (IOException e) {
			logger.error("Source schema not found: [{}]", schemaUrl, e);
			return null;
		}
	}
	
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
}
