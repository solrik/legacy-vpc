package com.ikea.dexf.vpc.app;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ch.qos.logback.access.tomcat.LogbackValve;
import io.swagger.api.RFC3339DateFormat;

@Configuration
@EnableWebMvc
@EnableCaching
public class AppConfig implements WebMvcConfigurer {

  @Bean
  public ServletWebServerFactory servletContainerFactory() {
    TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
    // Disable max keep alive request since we will always run behind load balancer
    factory.addConnectorCustomizers(
        connector -> ((AbstractHttp11Protocol<?>) connector.getProtocolHandler())
            .setMaxKeepAliveRequests(-1));
    // Change acceptor threads since we will always run on multi CPU
    factory.addConnectorCustomizers(
        connector -> ((AbstractHttp11Protocol<?>) connector.getProtocolHandler())
            .setAcceptorThreadCount(2));
    // Write access log to console.
    // Configuration is taken from file logback-access.xml in src/main/resources/conf
    factory.addContextValves(new LogbackValve());
    return factory;
  }
  
  @Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.dateFormat(new RFC3339DateFormat());
		builder.serializationInclusion(Include.NON_NULL);
		converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
		
		// used for application/xml content-type in ShoppingBagController
		converters.add(new StringHttpMessageConverter());
	}
	
  @Bean
  public CacheManager cacheManager() {
      return new EhCacheCacheManager(ehCacheCacheManager().getObject());
  }

  @Bean
  public EhCacheManagerFactoryBean ehCacheCacheManager() {
      EhCacheManagerFactoryBean factory = new EhCacheManagerFactoryBean();
      factory.setConfigLocation(new ClassPathResource("ehcache.xml"));
      factory.setShared(true);
      return factory;
  }
  
  @Bean
  public RestTemplate restTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
  	TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .setProtocol("TLSv1.2")
			        .build();
		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom()
              .setSSLSocketFactory(csf)
              .build();
		HttpComponentsClientHttpRequestFactory requestFactory =
              new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		requestFactory.setConnectTimeout(15000);
		requestFactory.setConnectionRequestTimeout(15000);
		requestFactory.setReadTimeout(15000);
		return new RestTemplate(requestFactory);
  }
  
}
