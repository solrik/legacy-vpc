package se.ikea.addonapps.vpcservice.exception;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Invalid content")
public class InvalidConfigurationContentException extends Exception {

	private static final long serialVersionUID = 1L;
	private String version;
	private String source;
	private String schemaUrl;
	private List<String> fields;
	
	private static final Logger logger = LoggerFactory.getLogger(InvalidConfigurationContentException.class);
	
	public InvalidConfigurationContentException(String source, String version, String schemaUrl) {
		this.source = source;
		this.version = version;
		this.schemaUrl = schemaUrl;
	}
	
	public InvalidConfigurationContentException(String source, String version) {
		this.source = source;
		this.version = version;
	}
	
	public InvalidConfigurationContentException(List<String> fields) {
		StringBuilder b = new StringBuilder();
		for (String s : fields)
			b.append(s).append(" ");
		logger.warn("Invalid [PostConfiguration] request. Missing fields [{}]", b.toString());
		this.fields = fields;
	}

	public InvalidConfigurationContentException(String source, String version, String schemaUrl, IllegalArgumentException e) {
		super(e);
		this.source = source;
		this.version = version;
		this.schemaUrl = schemaUrl;
	}

	public String getSchemaUrl() {
		return schemaUrl;
	}

	public void setSchemaUrl(String schemaUrl) {
		this.schemaUrl = schemaUrl;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	public List<String> getFields() {
		return fields;
	}

}
