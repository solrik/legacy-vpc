package se.ikea.addonapps.vpc.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import se.ikea.addonapps.vpc.common.util.AlertHelper;

@ResponseStatus(value=HttpStatus.SERVICE_UNAVAILABLE)
public class DbException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DbException(Exception e) {
		super(e);
		AlertHelper.DB_ALERT_LOGGER.error("Database connectivity error", e);
	}

}
