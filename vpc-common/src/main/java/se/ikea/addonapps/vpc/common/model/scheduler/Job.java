package se.ikea.addonapps.vpc.common.model.scheduler;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

@Entity
@Table(name = "vpcjob")
@JsonRootName(value = "job")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Job implements Serializable {
	
	private static final long serialVersionUID = 5273005959780402822L;
	
	public static final String STATUS_ACTIVE = "A";
	public static final String STATUS_PENDING = "P";
	public static final String STATUS_COMPLETED = "C";
	public static final String STATUS_FAILED = "F";
	
	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";
	public static final String METHOD_PUT = "PUT";
	public static final String METHOD_DELETE = "DELETE";

	@Id
	@Column(name = "job_id")
	@SequenceGenerator(name="job_seq",sequenceName="vpcjob_job_id_seq", allocationSize=1)
	@GeneratedValue(generator="job_seq")
	private Long id;
	
	@Column(name = "method")
	private String method;
	
	@Column(name = "url")
	private String url;

	@Column(name = "category")
	private String category;
	
	@Column(name = "status")
	private String status = STATUS_PENDING;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "created")
	private Timestamp created;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "started")
	private Timestamp started;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "finished")
	private Timestamp finished;
	
	@Column(name="jsonpayload")
	private String jsonPayload;

	public String getJsonPayload() {
		return jsonPayload;
	}

	public void setJsonPayload(String jsonPayload) {
		this.jsonPayload = jsonPayload;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getStarted() {
		return started;
	}

	public void setStarted(Timestamp started) {
		this.started = started;
	}

	public Timestamp getFinished() {
		return finished;
	}

	public void setFinished(Timestamp finished) {
		this.finished = finished;
	}

}
