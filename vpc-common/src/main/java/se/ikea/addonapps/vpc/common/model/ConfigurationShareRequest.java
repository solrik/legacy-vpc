package se.ikea.addonapps.vpc.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonRootName(value = "shareRequest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigurationShareRequest {

	private String retailUnit;
	private String langCode;
	private String template;
	private String mediaType;
	private String storeBuCode;
	private List<String> addrList = new ArrayList<String>();
	private String configId;
	
	@JsonGetter
	public String getRetailUnit() {
		return retailUnit;
	}
	
	@JsonSetter
	public void setRetailUnit(String retailUnit) {
		this.retailUnit = retailUnit;
	}
	
	@JsonGetter
	public String getLangCode() {
		return langCode;
	}
	
	@JsonSetter
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	
	@JsonGetter
	public List<String> getAddressList() {
		return addrList;
	}
	
	@JsonSetter
	public void setAddressList(List<String> addrList) {
		this.addrList = addrList;
	}
	
	@JsonIgnore
	public void addAddress(String addr) {
		addrList.add(addr);
	}
	
	@JsonSetter
	public void setConfigId(String configId) {
		this.configId = configId;
	}
		
	@JsonGetter
	public String getConfigId() {
		return configId;
	}
	
	@JsonGetter
	public String getMediaType() {
		return mediaType;
	}
	
	@JsonSetter
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	
	@JsonGetter
	public String getTemplate() {
		return template;
	}
	
	@JsonSetter
	public void setTemplate(String template) {
		this.template = template;
	}
	
	@JsonGetter
	public String getStoreBuCode() {
		return storeBuCode;
	}
	
	@JsonSetter
	public void setStoreBuCode(String storeBuCode) {
		this.storeBuCode = storeBuCode;
	}
}
