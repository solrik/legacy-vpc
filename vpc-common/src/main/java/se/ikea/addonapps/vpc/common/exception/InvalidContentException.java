package se.ikea.addonapps.vpc.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Invalid content")
public class InvalidContentException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(InvalidContentException.class);

	public InvalidContentException(String message) {
		super(message);
		logger.warn(message);
	}

	public InvalidContentException(String message, Throwable cause) {
		super(message, cause);
		logger.warn(message, cause);
	}

}
