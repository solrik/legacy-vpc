package se.ikea.addonapps.vpcservice.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import se.ikea.addonapps.vpc.common.util.AlertHelper;

@ResponseStatus(value=HttpStatus.SERVICE_UNAVAILABLE)
public class FileStorageException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(FileStorageException.class);

	public FileStorageException(String action, String filePath, Exception e) {
		super(e);
		String str = "Failed to [{}] file [{}]";
		AlertHelper.FILE_STORAGE_ALERT_LOGGER.error(str, action, filePath, e);
		logger.error(str, action, filePath, e);
	}
	
	public FileStorageException(String action, String filePath, int statusCode) {
		String str = "Failed to [{}] file [{}], status code [{}]";
		AlertHelper.FILE_STORAGE_ALERT_LOGGER.error(str, action, filePath, statusCode);
		logger.error(str, action, filePath, statusCode);
	}
	
}
