package se.ikea.addonapps.vpcservice.util;

public class Base28IDGenerator {

	public static final String CHARACTERS = "23456789BCDFGHJKLMNPQRSTVWYZ";
	public static final int LENGTH = CHARACTERS.length();

	public static long base28StringtoValue(String baseXString) {
		if (baseXString == null)
			return 0;
		
		char[] chars = baseXString.toUpperCase().toCharArray();

		long sum = 0;

		int multiplier = chars.length;
		for (int i = 0; i < chars.length; i++) {

			int index = CHARACTERS.indexOf(chars[i]);
			multiplier--;
			double base = Math.pow(LENGTH, multiplier);
			sum += base * index;
		}

		return sum;

	}

	public static String valueToBase28(long value) {

		long div = value / LENGTH;
		long rest = value % LENGTH;

		if (div > 0) {
			return valueToBase28(div) + valToChar(rest);
		} else {
			return String.valueOf(valToChar(rest));
		}
	}

	private static char valToChar(long value) {
		return (char) CHARACTERS.charAt((int) value);
	}
}
