package se.ikea.addonapps.vpcservice.util;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Base64;

import io.swagger.vpcservice.model.ConfigurationData;
import io.swagger.vpcservice.model.VpConfiguration;
import se.ikea.addonapps.vpcservice.model.Configuration;

public class ModelMapper {

	public static Configuration fromVpConfiguration(VpConfiguration vpConfig, String ru, String lc) {
		if (vpConfig == null)
			return null;
		Configuration config = new Configuration();
		config.setFriendlyId(vpConfig.getConfigurationId());
		config.setRetailUnit(ru);
		config.setLangCode(lc);
		config.setSource(vpConfig.getConfigurationSource());
		config.setVersion(vpConfig.getVersion());
		config.setContent(vpConfig.getConfigurationData().getContent());
		config.setSbsl(vpConfig.getConfigurationData().getShoppingBagSectionList());
		config.setContentType(vpConfig.getContentType());
		if (vpConfig.getFileSize() != null)
			config.setFileSize(vpConfig.getFileSize().longValue());
		if (vpConfig.getCreateDate() != null)
			config.setCreatedDate(new Timestamp(vpConfig.getCreateDate().getTime()));
		if (vpConfig.getLastAccessDate() != null)
			config.setAccessedDate(new Timestamp(vpConfig.getLastAccessDate().getTime()));
		
		return config;
	}
	
	public static VpConfiguration toVpConfiguration(Configuration config) {
		VpConfiguration vpConfig = new VpConfiguration();
		vpConfig.setConfigurationId(config.getFriendlyId());
		vpConfig.setConfigurationSource(config.getSource());
		vpConfig.setContentType(config.getContentType());
		vpConfig.setCreateDate(config.getCreatedDate());
		vpConfig.setLastAccessDate(config.getAccessedDate());
		vpConfig.setFileSize(new BigDecimal(config.getFileSize()));
		vpConfig.setVersion(config.getVersion());
		
		ConfigurationData cData = new ConfigurationData();
		cData.setContent(new String(Base64.getEncoder().encode(config.getContent().getBytes()), StandardCharsets.UTF_8));
		vpConfig.setConfigurationData(cData);
		return vpConfig;
	}
	
}
