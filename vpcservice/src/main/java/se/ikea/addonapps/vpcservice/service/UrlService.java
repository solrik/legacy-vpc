package se.ikea.addonapps.vpcservice.service;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.ikea.addonapps.vpcservice.config.AppPropertiesVpcService;
import se.ikea.addonapps.vpcservice.model.Configuration;

/**
 * Configurator deeplink format:
 * /addon-app/{app}/irw/latest/{ru}/{lc}/#/u/{spr}
 * 
 * E.g.
 * /addon-app/vallentuna/latest/se/sv/#/u/S12345678
 */
@Service
public class UrlService {
	
	private static final String CONFIG_URL_PATTERN = "/vpc/%s/%s/%s/configuration/%s"; 
	
	private static final String BAG_URL_PATTERN = "/vpcservice/retail/%s/%s/vpc/configuration/shoppingbag/%s";
	
	private static final Logger logger = LoggerFactory.getLogger(UrlService.class);
	
	@Autowired
	private AppPropertiesVpcService appProps;

	public String createConfiguratorDeepLink(Configuration config, String cc, String lc) {
		String[] localeOverride = validateCountryAndLanguage(config, cc, lc);
		String urlPattern = appProps.getUrlPatternDeeplink(config.getSource().toLowerCase(), cc);
		String formattedUrl = MessageFormat.format(urlPattern, config.getSource().toLowerCase(), localeOverride[0], localeOverride[1], config.getFriendlyId());
		logger.debug("Created deeplink URL for planner [{}]: [{}]", config.getSource(), formattedUrl);
		return formattedUrl;
	}
	
	public String createShortUrl(Configuration config, String cc, String lc) {
		String[] localeOverride = validateCountryAndLanguage(config, cc, lc);
		String urlPattern = appProps.getUrlPatternShortUrl(config.getSource().toLowerCase(), cc);
		String formattedUrl = MessageFormat.format(urlPattern, localeOverride[0], localeOverride[1], config.getSource().toLowerCase(), config.getFriendlyId());
		logger.debug("Created short URL for planner [{}]: [{}]", config.getSource(), formattedUrl);
		return formattedUrl;
	}
	
	private String[] validateCountryAndLanguage(Configuration config, String countryCode, String langCode) {
		String[] supportedLang = appProps.getSupportedLanguageCodes(countryCode.toUpperCase());
		if (isEmpty(supportedLang)) {
			logger.info("Supplied CountryCode [{}] is not supported. Falling back to original locale for ID [{}]: {}/{}", countryCode, config.getFriendlyId(), config.getRetailUnit(), config.getLangCode());
			countryCode = config.getRetailUnit();
			langCode = config.getLangCode();
		} else {
			boolean foundMatch = false;
			for (String lc : supportedLang) {
				if (lc.equals(langCode))
					foundMatch = true;
			}
			if (!foundMatch) {
				logger.info("Lang code [{}] is not supported in retail unit [{}]. Falling back to default [{}]", langCode, countryCode, supportedLang[0]);
				langCode = supportedLang[0];
			}
		}
		if (isEmpty(langCode) && !isEmpty(supportedLang)) {
			langCode = supportedLang[0];
		}
		return new String[]{countryCode, langCode};
	}
	
	public String createConfigurationUrl(Configuration config, String apiVersion) {
		return String.format(CONFIG_URL_PATTERN, apiVersion, config.getRetailUnit(), config.getLangCode(), config.getFriendlyId());
	}
	
	public String createShoppingBagUrl(String bagId, String countryCode, String langCode) {
		return String.format(BAG_URL_PATTERN, countryCode, langCode, bagId);
	}
	
	private boolean isEmpty(String[] s) {
		return (s == null || s.length == 0);
	}

	private boolean isEmpty(String s) {
		return (s == null || s.trim().length() == 0);
	}

}
