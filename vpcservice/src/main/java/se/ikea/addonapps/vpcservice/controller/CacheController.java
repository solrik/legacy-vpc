package se.ikea.addonapps.vpcservice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.ehcache.Ehcache;

@Controller
@RequestMapping("/cache")
public class CacheController {

	@Autowired
	private CacheManager cacheMgr;
	
	@RequestMapping("/{cacheName}/purge")
	//@CacheEvict(value = cacheName, allEntries = true)
	public ResponseEntity<?> purgeCache(@PathVariable String cacheName) {
		Cache c = cacheMgr.getCache(cacheName);
		if (c != null) {
			Ehcache nativeCache = (Ehcache)c.getNativeCache();
			int size = nativeCache.getSize();
			c.clear();
			return new ResponseEntity<String>("Cleared "+size+" entries.", HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getCaches(HttpServletResponse response) {
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		EhCacheCacheManager ehCacheMgr = (EhCacheCacheManager)cacheMgr;
		Map<String,String> map = new HashMap<>();
		for (String cacheName : ehCacheMgr.getCacheNames()) {
			map.put(cacheName, "/vpcservice/cache/"+cacheName);
		}
		return new ResponseEntity<String>(marshalToJson(map, true), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{cacheName}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCacheInfo(@PathVariable String cacheName, HttpServletResponse response) {
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		Cache c = cacheMgr.getCache(cacheName);
		Map<String, Object> map = new HashMap<>();

		if (c != null) {
			Ehcache nativeCache = (Ehcache)c.getNativeCache();
			map.put("name", cacheName);
			map.put("heapSize", nativeCache.getStatistics().getLocalHeapSize());
			map.put("maxEntries", nativeCache.getCacheConfiguration().getMaxEntriesLocalHeap());
			map.put("hitCount", nativeCache.getStatistics().cacheHitCount());
			map.put("missCount", nativeCache.getStatistics().cacheMissCount());
			List<Object> cacheKeyList = new ArrayList<>();
			map.put("cacheKeys", cacheKeyList);

			for (Object key : nativeCache.getKeys()) {
				cacheKeyList.add(key);
			}
		} else {
			return new ResponseEntity<String>("Cache "+cacheName+" not found.", HttpStatus.OK);
		}	
		return new ResponseEntity<String>(marshalToJson(map, true), HttpStatus.OK);	
	}
	
	private static String marshalToJson(Object o, boolean pretty) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			if (pretty)
				json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
			else
				json = mapper.writeValueAsString(o);
			return json;
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
			return null;
		}
	}
}
