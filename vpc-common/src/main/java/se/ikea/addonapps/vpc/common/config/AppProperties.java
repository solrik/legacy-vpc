package se.ikea.addonapps.vpc.common.config;

import java.util.PropertyResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.ikea.addonapps.vpc.common.exception.UnexpectedException;
import se.ikea.addonapps.vpc.common.util.AlertHelper;

public abstract class AppProperties {

	private static final Logger logger = LoggerFactory.getLogger(AppProperties.class);
	
	private static final String PROP_DB_HIBERNATE_DIALECT = "db_jpa_hibernate_dialect";
	private static final String PROP_DB_HIBERNATE_HBM2DDL_AUTO = "db_jpa_hibernate_hbm2ddl_auto";
	private static final String PROP_DB_JDBC_DRIVER_CLASSNAME = "db_jdbc_driverclassname";
	private static final String PROP_DB_JDBC_URL = "db_jdbc_url";
	private static final String PROP_DB_USERNAME = "db_username";
	private static final String PROP_DB_PASSWORD = "db_password";
	private static final String PROP_DB_VENDOR = "db_jpa_database_vendor";
	
	private PropertyResourceBundle appProps;
	private String env;
	
	public AppProperties() {
		env = System.getProperty("env");
		if (env == null) {
			alert("Required JVM property [env] is null.");
			return;
		}
		logger.info("JVM property [env] set to [{}]", env);
		try {
			appProps = (PropertyResourceBundle)PropertyResourceBundle.getBundle("config_"+env);
		} catch (Exception e) {
			throw new UnexpectedException("Unable to find property file config_"+env, e);
		}
	}
	
	public String getEnv() {
		return env;
	}
	
	public String getProperty(String key) {
		String s = System.getenv(key);
		if (s == null) {
			if (!appProps.containsKey(key))
				return null;
			s = appProps.getString(key);
		}
		
		logger.debug("Property [{}] = [{}]", key, (key != null && key.contains("assword")) ? "******" : s);
		return s;
	}
	
	public String getDbJpaHibernateDialect() {
		return getProperty(PROP_DB_HIBERNATE_DIALECT);
	}
	
	public String getDbJpaHibernateHb22DdlAuto() {
		return getProperty(PROP_DB_HIBERNATE_HBM2DDL_AUTO);
	}
	
	public String getDbJdbcDriverClassName() {
		return getProperty(PROP_DB_JDBC_DRIVER_CLASSNAME);
	}
	
	public String getDbJdbcUrl() {
		return getProperty(PROP_DB_JDBC_URL);
	}
	
	public String getDbUserName() {
		return getProperty(PROP_DB_USERNAME);
	}
	
	public String getDbPassword() {
		return getProperty(PROP_DB_PASSWORD);
	}
	
	public String getDbVendor() {
		return getProperty(PROP_DB_VENDOR);
	}
	
	protected void alert(String err) {
		AlertHelper.CONFIG_ALERT_LOGGER.error(err);
		logger.error(err);
	}
	
	protected void alert(String err, Exception e, String...params) {
		AlertHelper.CONFIG_ALERT_LOGGER.error(err, e, params);
		logger.error(err, e, params);
	}
}
