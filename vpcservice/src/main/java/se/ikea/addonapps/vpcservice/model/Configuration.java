package se.ikea.addonapps.vpcservice.model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Base64;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import msc.selling.common.shoppingbag._2.ShoppingBagSectionListComplexType;
import se.ikea.addonapps.vpcservice.util.Base28IDGenerator;
import se.ikea.addonapps.vpcservice.util.JaxbHelper;

/**
 * @author Andreas , shashi
 *
 */
@Entity
@Table(name = "vpcconfig")
public class Configuration {

	@Id
	@Column(name = "config_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Transient
	private String friendlyId;

	@Column(name = "file_path")
	private String filePath;

	@Column(name = "file_size")
	private long fileSize;

	@Column(name = "source")
	private String source;

	@Column(name = "hash")
	private String hash;

	@Column(name = "version")
	private String version;

	@Column(name = "created_timestamp")
	private Timestamp createdDate;

	@Column(name = "accessed_timestamp")
	private Timestamp accessedDate;

	@Column(name = "retail_unit")
	private String retailUnit;

	@Column(name = "lang_code")
	private String langCode;

	@Transient
	private String contentType;

	@Column(name = "content", length=32768)
	private String content;

	@Column(name = "sbsl", length=32768)
	private String sbsl;

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the configId
	 */
	public Long getId() {
		return id;
	}

	public void setFriendlyId(String friendlyId) {
		this.friendlyId = friendlyId;
	}

	/**
	 * @return the friendlyId
	 */
	public String getFriendlyId() {
		if (friendlyId != null) {
			return friendlyId;
		}
		return Base28IDGenerator.valueToBase28(id);
	}

	/**
	 * @return the filePath
	 */
	public String getAbsoluteFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setAbsoluteFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileSize
	 */
	public long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the createdDate
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the accessedDate
	 */
	public Timestamp getAccessedDate() {
		return accessedDate;
	}

	/**
	 * @param accessedDate the accessedDate to set
	 */
	public void setAccessedDate(Timestamp accessedDate) {
		this.accessedDate = accessedDate;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	public void setRetailUnit(String retailUnit) {
		this.retailUnit = retailUnit;
	}

	public String getRetailUnit() {
		return retailUnit;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setSbsl(String xml) {
		sbsl = xml;
	}

	public String getSbsl() {
		return sbsl;
	}

	public ShoppingBagSectionListComplexType getShoppingBagSectionListXml()
			throws JAXBException, IOException, ParserConfigurationException, SAXException {
		return JaxbHelper.unmarshall(ShoppingBagSectionListComplexType.class,
				new String(Base64.getDecoder().decode(sbsl), StandardCharsets.UTF_8));
	}

}
