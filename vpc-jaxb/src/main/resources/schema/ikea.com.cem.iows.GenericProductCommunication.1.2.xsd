<!-- 
Change log:

2011-11-11 Christian Mårtensson	Version P1.0-1, Initial version
2011-12-22 Christian Mårtensson	Version P1.0-2, Updated GenericProductCommunicationComplexType
																			Removed GenericProductCommunicationRefComplexType, GenericProductCommunicationRefListComplexType
2012-04-13 Christian Mårtensson	Version P1.0-3, IKEA00746353: Added GPRCommSelectionCriteriaListComplexType and its sub types
																			Added GPRCommSelectionCriteriaSelectionListComplexType and its subtypes
																			Updated GenericProductCommunicationComplexType
2012-05-02 Christian Mårtensson	Version P1.0-4, Added SelectionCriteriaNameSimpleType, SelectionCriteriaValueSimpleType from Product CEM GenericProductLocal.
																			Updated GPRCommSelectionCriteriaSelectionComplexType to refer to the new types.
2012-07-02 Brook Abraha			Version P1.0-5, IKEA00756362: Changed reference to sortNo to ProductCommunicationLocal instead of RetailItemCommm and removed import for the same.
																			Added version to the schema
2012-09-27 Brook Abraha			Version P1.0-6,	Changed GPRCommSelectionCriteriaList to optional. GPRs with no valid selection criterias, will be filtered out and this element will then not be set							
2012-11-22 Fredrik Stenberg		Version P1.1-1, Updated to use new version of Catalog XSD									
2013-03-12 Fredrik Stenberg		Version P1.2-1, Added CatalogElementRelationList and updated to use new version of Catalog XSD									
-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:gpc="ikea.com/cem/iows/GenericProductCommunication/1.0/" xmlns:ca="ikea.com/cem/iows/Catalog/2.0/" xmlns:irr="ikea.com/cem/Retail/Range/1.2/" xmlns:ipcl="ikea.com/cem/ProductCommunicationLocal/1.0/" targetNamespace="ikea.com/cem/iows/GenericProductCommunication/1.0/" elementFormDefault="unqualified" attributeFormDefault="unqualified" version="1.3.1">
	<xs:import namespace="ikea.com/cem/iows/Catalog/2.0/" schemaLocation="ikea.com.cem.iows.Catalog.2.3.xsd"/>
	<xs:import namespace="ikea.com/cem/Retail/Range/1.2/" schemaLocation="ikea.com.cem.Retail.Range.1.2.xsd"/>
	<xs:import namespace="ikea.com/cem/ProductCommunicationLocal/1.0/" schemaLocation="ikea.com.cem.ProductCommunicationLocal_Without_Annotation.1.0.xsd"/>
	<xs:complexType name="GenericProductCommunicationComplexType">
		<xs:sequence>
			<xs:element name="GenericProductNo" type="ipcl:GenericProductNoSimpleType"/>
			<xs:element name="GlobalisationContext" type="irr:GlobalisationContextComplexType"/>
			<xs:element name="ClassUnitKey" type="irr:ClassUnitKeyComplexType"/>
			<xs:element name="ProductName" type="ipcl:ProductNameSimpleType"/>
			<xs:element name="ProductTypeName" type="ipcl:ProductTypeNameSimpleType"/>
			<xs:element name="GPRCommSelectionCriteriaList" type="gpc:GPRCommSelectionCriteriaListComplexType" minOccurs="0"/>
			<xs:element name="GPRCommItemRefList" type="ca:ItemRefListComplexType"/>
			<xs:element name="CatalogRefList" type="ca:CatalogRefListComplexType" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="CatalogElementRelationList" type="ca:CatalogElementRelationListComplexType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GenericProductCommunicationList">
		<xs:sequence>
			<xs:element name="GenericProductCommunication" type="gpc:GenericProductCommunicationComplexType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GPRCommSelectionCriteriaListComplexType">
		<xs:sequence>
			<xs:element name="GPRCommSelectionCriteria" type="gpc:GPRCommSelectionCriteriaComplexType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GPRCommSelectionCriteriaComplexType">
		<xs:sequence>
			<xs:element name="SelectionCriteriaCode" type="gpc:SelectionCriteriaCodeSimpleType"/>
			<xs:element name="SelectionCriteriaName" type="xs:string"/>
			<!-- CRC CEM type -->
			<xs:element name="SortNo" type="ipcl:SortNoSimpleType"/>
			<xs:element name="GPRCommSelectionCriteriaValueList" type="gpc:GPRCommSelectionCriteriaValueListComplexType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GPRCommSelectionCriteriaValueListComplexType">
		<xs:sequence>
			<xs:element name="GPRCommSelectionCriteraValue" type="gpc:GPRCommSelectionCriteriaValueComplexType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GPRCommSelectionCriteriaValueComplexType">
		<xs:sequence>
			<xs:element name="SelectionCriteriaValue" type="xs:string"/>
			<!-- CRC CEM type -->
			<xs:element name="ItemRefList" type="ca:ItemRefListComplexType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GPRCommSelectionCriteriaSelectionListComplexType">
		<xs:sequence>
			<xs:element name="GPRCommSelectionCriteriaSelection" type="gpc:GPRCommSelectionCriteriaSelectionComplexType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GPRCommSelectionCriteriaSelectionComplexType">
		<xs:sequence>
			<xs:element name="SelectionCriteriaCode" type="gpc:SelectionCriteriaCodeSimpleType"/>
			<xs:element name="SelectionCriteriaName" type="gpc:SelectionCriteriaNameSimpleType"/>
			<xs:element name="SelectionCriteriaValue" type="gpc:SelectionCriteriaValueSimpleType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="SelectionCriteriaCodeSimpleType">
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
	<xs:simpleType name="SelectionCriteriaNameSimpleType">
		<xs:annotation>
			<xs:documentation>Name of the selection criteria e.g. Size, Colour</xs:documentation>
			<xs:appinfo source="CemAppInfoContentDataType">
				<CemAppInfoContentDataType>
					<DescriptionData>
						<BusinessShort/>
						<BusinessLong/>
						<Development>Name of the selection criteria e.g. Size, Colour</Development>
					</DescriptionData>
					<CemRefData>
						<CemRefName>SelectionCriteriaName</CemRefName>
						<CemRefType>DataType</CemRefType>
						<CemRefRevision/>
					</CemRefData>
					<AliasNameData/>
				</CemAppInfoContentDataType>
			</xs:appinfo>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:maxLength value="50"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="SelectionCriteriaValueSimpleType">
		<xs:annotation>
			<xs:documentation>Value of the selection criteria e.g. natural colour</xs:documentation>
			<xs:appinfo source="CemAppInfoContentDataType">
				<CemAppInfoContentDataType>
					<DescriptionData>
						<BusinessShort/>
						<BusinessLong/>
						<Development>Value of the selection criteria e.g. natural colour</Development>
					</DescriptionData>
					<CemRefData>
						<CemRefName>SelectionCriteriaValue</CemRefName>
						<CemRefType>DataType</CemRefType>
						<CemRefRevision/>
					</CemRefData>
					<AliasNameData/>
				</CemAppInfoContentDataType>
			</xs:appinfo>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:maxLength value="1000"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>
