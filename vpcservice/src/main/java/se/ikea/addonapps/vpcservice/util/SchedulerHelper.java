package se.ikea.addonapps.vpcservice.util;

import java.io.IOException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import se.ikea.addonapps.vpc.common.exception.SchedulingException;
import se.ikea.addonapps.vpc.common.model.scheduler.Job;
import se.ikea.addonapps.vpc.common.util.JsonHelper;

public class SchedulerHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerHelper.class);

	public static Job postJob(String postJobUrl, String schedulerCategory, String jobExecutionUrl, String jsonPayload) throws SchedulingException {
		long startTime = System.currentTimeMillis();
		RestTemplate client = new RestTemplate();
		Job job = new Job();
		job.setCategory(schedulerCategory);
		job.setMethod(Job.METHOD_POST);
		job.setUrl(jobExecutionUrl);
		if (jsonPayload != null) {
			String payloadBase64 = new String(Base64.getEncoder().encode(jsonPayload.getBytes()));
			job.setJsonPayload(payloadBase64);
		}
		ObjectMapper mapper = new ObjectMapper();
		String payload = JsonHelper.marshalToJson(job, false);

		HttpEntity<String> entity = new HttpEntity<String>(payload, createHeaders());

		logger.debug("PostJob {} with payload {}", postJobUrl, payload);
		
		ResponseEntity<String> response = null;
		try {
			response = client.exchange(postJobUrl, HttpMethod.POST, entity, String.class);
			if (!response.getStatusCode().is2xxSuccessful()) {
				throw new SchedulingException(payload, response.getStatusCode().value(), response.getStatusCode().getReasonPhrase());
			}
		} catch (RestClientException e) {
			logger.error("PostJob: Error posting job to url {}: {}", postJobUrl, payload, e);
			throw new SchedulingException(payload, e);
		}
		
		try {
			job = mapper.readValue(response.getBody(), Job.class);
		} catch (IOException e) {
			logger.error("PostJob: Invalid JSON response returned from url {}: {}", postJobUrl, response.getBody(), e);
			throw new SchedulingException(payload, e);
		}
		
		long stopTime = System.currentTimeMillis();
		logger.info("Successful [PostJob] {} in [{}] ms.", postJobUrl, stopTime-startTime);
		return job;
	}
	
	static HttpHeaders createHeaders() {
		return new HttpHeaders() {private static final long serialVersionUID = 1L;
			{
				set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
		    }
		};
	}
}
