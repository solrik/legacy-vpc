package se.ikea.addonapps.vpcservice.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import msc.selling.common.shoppingbag._2.ShoppingBagItemComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagItemListComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagSectionComplexType;
import msc.selling.common.shoppingbag._2.ShoppingBagSectionListComplexType;
import se.ikea.addonapps.vpc.common.exception.SchedulingException;
import se.ikea.addonapps.vpc.common.exception.UnexpectedException;
import se.ikea.addonapps.vpc.common.model.ConfigurationShareRequest;
import se.ikea.addonapps.vpc.common.model.scheduler.Job;
import se.ikea.addonapps.vpc.common.util.JsonHelper;
import se.ikea.addonapps.vpc.common.util.RestHelper;
import se.ikea.addonapps.vpc.common.util.Sha256Generator;
import se.ikea.addonapps.vpcservice.config.AppPropertiesVpcService;
import se.ikea.addonapps.vpcservice.exception.InvalidConfigurationContentException;
import se.ikea.addonapps.vpcservice.exception.ResourceNotFoundException;
import se.ikea.addonapps.vpcservice.jpa.service.VPCConfigurationPersistenceService;
import se.ikea.addonapps.vpcservice.model.Configuration;
import se.ikea.addonapps.vpcservice.util.Base28IDGenerator;
import se.ikea.addonapps.vpcservice.util.JaxbHelper;
import se.ikea.addonapps.vpcservice.util.SchedulerHelper;

@Service
public class ConfigurationService {
	
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationService.class);
	
	@Autowired
	private VPCConfigurationPersistenceService configPersistenceService; 
	
	@Autowired
	private AppPropertiesVpcService appProps;
	
	@Autowired
	private JsonSchemaValidatorService schemaValidator;
	

	public Configuration getConfigurationByFriendlyId(String friendlyId) {
		Configuration config = configPersistenceService.findById(Base28IDGenerator.base28StringtoValue(friendlyId));
		if (config == null)
			return null;
		
		config.setAccessedDate(new Timestamp(System.currentTimeMillis()));
		configPersistenceService.persist(config);
		return config;		
	}
	
	@Transactional
	public Configuration store( String retailUnit, String language, Configuration config) throws InvalidConfigurationContentException {
		logger.debug("About to store content {}",config.getContent());
		
		String schemaFileName = appProps.getSourceSchemaFileNameByNameAndVersion(config.getSource().toLowerCase(),config.getVersion());
		if (schemaFileName == null) {
			logger.warn("No JSON schema found for configuration source [{}] in version [{}]", config.getSource(), config.getVersion());
			throw new InvalidConfigurationContentException(config.getSource(), config.getVersion());
		}
		
		String content = null;
		try {
			content = new String(Base64.getDecoder().decode(config.getContent()), StandardCharsets.UTF_8);
			boolean jsonValid = schemaValidator.isJsonValid(schemaFileName, content);
			if (!jsonValid) {
				throw new InvalidConfigurationContentException(config.getSource(), config.getVersion(), schemaFileName);
			}
		} catch(IllegalArgumentException e) {
			logger.warn("Unable to decode base64 [{}]. Source [{}]", config.getContent(), config.getSource());
			throw new InvalidConfigurationContentException(config.getSource(), config.getVersion(), schemaFileName, e);
		}
		
		// generate has from the content + shopping bag information
		final String fileHash = generateHash(config);
		Configuration existingConfig = configPersistenceService.findByHash(fileHash);
		if (existingConfig != null) {
			logger.info("Configuration already exists [{}]. Will return existing", existingConfig.getFriendlyId());
			return existingConfig;
		} else {
			config.setFileSize(content.length());
			config.setHash(fileHash);
			Timestamp now = new Timestamp(new Date().getTime());
			config.setCreatedDate(now);
			config.setAccessedDate(now);
			configPersistenceService.persist(config);
		}
		
		return config;
	}

	@Transactional
	public void delete(String configurationId, String ru, String lc) {
		Configuration config = getConfigurationByFriendlyId(configurationId);
		if (config == null)
			throw new ResourceNotFoundException();

		configPersistenceService.deleteById(config.getId());
	}
	
	/**
	 * Will only populate the parts of the configuration that are persisted in the local DB. Content stored on file in the Storage service will hence not be populated: ShoppingBag and configuration file data.
	 * @param friendlyId
	 * @return
	 */
	public Configuration getConfigurationHead(String friendlyId) {
		return configPersistenceService.findById(Base28IDGenerator.base28StringtoValue(friendlyId));
	}

	public Long getConfigurationCount() {
		return configPersistenceService.countConfigurations();
	}

	public Configuration getLatestConfiguration() {
		return configPersistenceService.getLatestConfiguration();
	}
	
	public Job share(ConfigurationShareRequest req) throws SchedulingException {
		String postJobUrl = String.format(RestHelper.SCHEDULER_POST_JOB_URL_FORMAT, appProps.getSchedulerServiceHostName(), appProps.getSchedulerServicePort());
		String shareExecUrl = String.format(RestHelper.SHARE_EMAIL_POST_URL, 
				appProps.getBackofficeServiceHostName(), 
				appProps.getBackofficeServicePort(), 
				req.getRetailUnit(), 
				req.getLangCode());
		
		String json = JsonHelper.marshalToJson(req, false);
		logger.debug("Marshalled ConfigurationShareRequest object to json: {}", json);
		
		String shareSaveEmailSchCategory = appProps.getSchCategoryShareSaveEmail();
		return SchedulerHelper.postJob(postJobUrl, shareSaveEmailSchCategory, shareExecUrl, json);
	}

	/**
	 * @param config
	 * @return
	 */
	private String generateHash(Configuration config){
		ShoppingBagSectionListComplexType sectionList;
		try {
			String sbSlDecoded = new String(Base64.getDecoder().decode(config.getSbsl()), StandardCharsets.UTF_8);
			sectionList = JaxbHelper.unmarshall(ShoppingBagSectionListComplexType.class, sbSlDecoded);
		} catch (JAXBException | IOException | ParserConfigurationException | SAXException e) {
			throw new UnexpectedException(e);
		}
		
		List<String> partNumbers = new ArrayList<>();
		for(ShoppingBagSectionComplexType section : sectionList.getShoppingBagSection()){
			ShoppingBagItemListComplexType itemList = section.getShoppingBagItemList();
			for(ShoppingBagItemComplexType item: itemList.getShoppingBagItem()){
				StringBuilder partNumber = new StringBuilder();
				partNumber.append(item.getItemType()).append(item.getItemNo()).append(item.getItemQty());
				partNumbers.add(partNumber.toString());
			}
		}
		// sort so that the order of articles in the bag doesnt generate a unique hash
		Collections.sort(partNumbers);
		StringBuilder sb = new StringBuilder(config.getContent());
		partNumbers.forEach(sb::append);
		return Sha256Generator.getSha256(sb.toString());
	}

}
