package se.ikea.addonapps.vpc.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import se.ikea.addonapps.vpc.common.util.AlertHelper;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Scheduler unavailable, consumer must retry transaction")
public class SchedulingException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulingException.class);
	
	public SchedulingException(String jsonPayload, int statusCode, String statusMessage) {
		String msg = "Failed to schedule job [{}] with status code [{}] and message [{}]";
		AlertHelper.ASYNC_JOB_ERROR_ALERT_LOGGER.error(msg, jsonPayload, statusCode, statusMessage);
		logger.error(msg);
	}

	public SchedulingException(String jsonPayload, Throwable cause) {
		super(cause);
		String msg = "Failed to schedule job [{}]";
		AlertHelper.ASYNC_JOB_ERROR_ALERT_LOGGER.error(msg, jsonPayload, cause);
		logger.error(msg, jsonPayload, cause);
	}
}
