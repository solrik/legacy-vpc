package se.ikea.addonapps.vpc.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonRootName("emailMessage")
public class EmailMessage {

	private String from;
	private List<String> to;
	private String refId;
	private EmailContent content;
	
	@JsonGetter
	public String getFrom() {
		return from;
	}
	
	@JsonSetter
	public void setFrom(String from) {
		this.from = from;
	}
	
	@JsonGetter
	public List<String> getTo() {
		return to;
	}
	
	@JsonSetter
	public void setTo(List<String> to) {
		this.to = to;
	}
	
	@JsonGetter
	public String getRefId() {
		return refId;
	}
	
	@JsonSetter
	public void setRefId(String refId) {
		this.refId = refId;
	}
	
	@JsonGetter
	public EmailContent getContent() {
		return content;
	}
	
	@JsonSetter
	public void setContent(EmailContent content) {
		this.content = content;
	}
}
